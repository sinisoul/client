package com.jagex;

/**
 * Created by hadyn on 5/19/2015.
 */
public class WrappedByteArray extends AbstractByteArray {

    private byte[] bytes;

    @Override
    public void put(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public byte[] get() {
        return bytes;
    }

    public static WrappedByteArray create(byte[] bytes) {
        WrappedByteArray arr = new WrappedByteArray();
        arr.put(bytes);
        return arr;
    }
}

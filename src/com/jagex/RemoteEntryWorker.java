package com.jagex;

import java.io.IOException;

/**
 * Created by hadyn on 5/19/2015.
 */
public class RemoteEntryWorker {
    public static final int REQUEST_LIMIT = 20;

    public static final int ERROR_TIMEOUT = 1001;
    public static final int ERROR_IO = 1002;

    public static final int RS_CLIENT_TYPE = 3;

    public static final int OP_REQ = 0;
    public static final int OP_PRIORITY_REQ = 1;
    public static final int OP_CLIENT_ONLINE = 2;
    public static final int OP_CLIENT_OFFLINE = 3;
    public static final int OP_ENCRYPTION_KEY = 4;
    public static final int OP_CLIENT_TYPE = 6;
    public static final int OP_RESET_QUEUE = 7;

    private SocketStream stream;
    private int attempts;
    private int status;

    private Buffer requestBuffer = new Buffer(4);
    private Buffer buf = new Buffer(8);

    private Queue<RemoteEntryRequest> pendingPriorityRequests = new Queue<>();
    private Queue<RemoteEntryRequest> priorityRequests = new Queue<>();

    private Queue<RemoteEntryRequest> pendingNormalRequests = new Queue<>();
    private Queue<RemoteEntryRequest> normalRequests = new Queue<>();

    private RemoteEntryRequest current;

    private byte encryptionKey = 0;

    public void incrementAttempts() {
        attempts++;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    // 2.5MB download speed maximum :^)
    public boolean process() {

        // If the stream is not initialized and we have requests, flag that
        // we need to connect to the update server.
        if(stream == null) {
            return priorityRequestCount() != 0 || normalRequestCount() != 0;
        }

        try {
            // Write all of the pending priority requests
            for(RemoteEntryRequest r = pendingPriorityRequests.first(); r != null; r = pendingPriorityRequests.next()) {
                System.out.println("Requesting " + r.getIndexId() + " " + r.getEntryId());
                requestBuffer.position(0);
                requestBuffer.putInt8(OP_PRIORITY_REQ);
                requestBuffer.putInt8(r.getIndexId());
                requestBuffer.putInt16(r.getEntryId());
                stream.write(requestBuffer.array(), 0, 4);
                priorityRequests.add(r);
            }

            for(RemoteEntryRequest r = pendingNormalRequests.first(); r != null; r = pendingNormalRequests.next()) {
                requestBuffer.position(0);
                requestBuffer.putInt8(OP_REQ);
                requestBuffer.putInt8(r.getIndexId());
                requestBuffer.putInt16(r.getEntryId());
                stream.write(requestBuffer.array(), 0, 4);
                normalRequests.add(r);
            }

            for(int i = 0; i < 100; i++) {
                int avail = stream.available();

                if(avail < 0) {
                    throw new IOException("End of stream");
                }

                if(avail == 0) {
                    break;
                }

                int len = 0;
                if(current != null) {
                    if(current.position() == 0) {
                        len = 1;
                    }
                } else {
                    len = 8;
                }

                // Read the chunk payload
                if(len < 1) {
                    Buffer buffer = current.getBuffer();

                    // Calculate the amount of bytes to read for the chunk
                    int entryLen = current.getLength() - current.getTrailerSize();
                    int read = 512 - current.position();
                    if(read >  entryLen - buffer.position()) {
                        read = entryLen - buffer.position();
                    }

                    if(read > avail) {
                        read = avail;
                    }

                    // Read the chunk bytes to the request buffer, if the encryption hash is set decrypt the bytes
                    stream.read(buffer.array(), buffer.position(), read);
                    if(encryptionKey != 0) {
                        buffer.xor(encryptionKey, buffer.position(), read);
                    }
                    current.position(current.position() + read);
                    buffer.skip(read);

                    if(buffer.position() == entryLen) {
                        System.out.println("Finished receiving " + current.getIndexId() + ", " + current.getEntryId());
                        current.setComplete(true);
                        current.unlinkSubNode();
                        current = null;
                    } else {
                        if(current.position() == 512) {
                            current.position(0);
                        }
                    }
                } else {
                    int read = len - buf.position();
                    if(read > avail) {
                        read = avail;
                    }

                    // Read the bytes to the header buffer, if the encryption hash is set
                    // decrypt the read bytes with the encryption hash
                    stream.read(buf.array(), buf.position(), read);
                    if(encryptionKey != 0) {
                        buf.xor(encryptionKey, buf.position(), read);
                    }
                    buf.skip(read);

                    if(buf.position() >= len) {
                        if(current != null) {
                            if(current.position() != 0) {
                                throw new IOException("Unexpected position");
                            }

                            // Read the chunk check, if its invalid invalidate the current request
                            if(buf.getInt8(0) != -1) {
                                current = null;
                            } else {
                                current.position(1);
                                buf.position(0);
                            }
                        } else {
                            buf.position(0);

                            // Read the response header
                            int indexId = buf.getUInt8();
                            int entryId = buf.getUInt16();
                            int flags = buf.getUInt8();
                            int packedSize = buf.getInt32();

                            int compression = flags & 0x7f;
                            boolean normal = (flags & 0x80) != 0;

                            // Search for the corresponding request
                            RemoteEntryRequest request = null;
                            if(!normal) {
                                for(RemoteEntryRequest req = priorityRequests.first(); req != null; req = priorityRequests.next()) {
                                    if(req.getIndexId() == indexId && req.getEntryId() == entryId) {
                                        request = req;
                                        break;
                                    }
                                }
                            } else {
                                for(RemoteEntryRequest req = normalRequests.first(); req != null; req = normalRequests.next()) {
                                    if(req.getIndexId() == indexId && req.getEntryId() == entryId) {
                                        request = req;
                                        break;
                                    }
                                }
                            }

                            if(request == null) {
                                throw new IOException("No such request exists");
                            }

                            // Write the container header from the response header
                            Buffer buffer = new Buffer(packedSize + (compression != 0 ? 9 : 5) + request.getTrailerSize());
                            buffer.putInt8(compression);
                            buffer.putInt32(packedSize);

                            request.setBuffer(buffer);
                            request.position(8);

                            // Set the current request
                            current = request;

                            // Reset the buffer position
                            buf.position(0);
                        }
                    }
                }
            }
        } catch(IOException ex) {
            ex.printStackTrace();
            try {
                stream.close();
            } catch (Throwable t) {

            }

            status = -2;                // Constant?
            ++this.attempts;
            stream = null;
            return priorityRequestCount() != 0 || normalRequestCount() != 0;
        }

        return false;
    }

    public void setupConnection(SocketStream s, boolean online) {

        System.out.println("Connecting");

        if(stream != null) {
            stream.close();
            stream = null;
        }

        stream = s;

        // Write the client type
        writeClientType();

        // Write the online status
        writeOnlineStatus(online);

        // Move all of the active priority requests to the pending priority request queue
        RemoteEntryRequest request;
        while((request = priorityRequests.poll()) != null) {
            pendingPriorityRequests.add(request);
        }

        // Move all of the active requests to the pending request queue
        while((request = normalRequests.poll()) != null) {
            pendingNormalRequests.add(request);
        }

        // If the encryption hash is set write it
        if(encryptionKey != 0) {
            writeEncryptionKey();
        }
    }

    public void writeOnlineStatus(boolean online) {
        if(stream != null) {
            try {
                requestBuffer.position(0);
                requestBuffer.putInt8(online ? OP_CLIENT_ONLINE : OP_CLIENT_OFFLINE);
                requestBuffer.putInt24(0);
                stream.write(requestBuffer.array(), 0, 4);
            } catch (IOException ex) {
                try {
                    stream.close();
                } catch(Throwable t) {

                }
                attempts++;
                stream = null;
                status = -2;            // TODO: Constant?
            }
        }
    }

    // NOT ENTIRELY SURE ABOUT THIS BUT YOLO
    public void writeClientType() {
        if(stream != null) {
            try {
                requestBuffer.position(0);
                requestBuffer.putInt8(OP_CLIENT_TYPE);
                requestBuffer.putInt24(RS_CLIENT_TYPE);
                stream.write(requestBuffer.array(), 0, 4);
            } catch (IOException ex) {
                try {
                    stream.close();
                } catch(Throwable t) {

                }
                attempts++;
                stream = null;
                status = -2;            // TODO: Constant?
            }
        }
    }

    public void writeEncryptionKey() {
        if(stream != null) {
            try {
                requestBuffer.position(0);
                requestBuffer.putInt8(OP_ENCRYPTION_KEY);
                requestBuffer.putInt8(encryptionKey);
                requestBuffer.putInt16(0);
                stream.write(requestBuffer.array(), 0, 4);
            } catch (IOException ex) {
                try {
                    stream.close();
                } catch(Throwable t) {

                }
                attempts++;
                stream = null;
                status = -2;            // TODO: Constant?
            }
        }
    }

    public void writeResetQueue() {}

    public RemoteEntryRequest request(int index, int entry, boolean priority) {
        return request(index, entry, 0, priority);
    }

    public RemoteEntryRequest request(int indexId, int entryId, int trailerSize, boolean priority) {
        RemoteEntryRequest request = new RemoteEntryRequest(indexId, entryId, trailerSize);
        request.setPriority(priority);
        if(priority) {
            if(isPriorityQueueFull()) {
                throw new RuntimeException("Reached priority request limit");
            }
            pendingPriorityRequests.add(request);
        } else {
            if(isNormalQueueFull()) {
                throw new RuntimeException("Reached normal request limit");
            }
            pendingNormalRequests.add(request);
        }
        return request;
    }

    public int priorityRequestCount() {
        return pendingPriorityRequests.size() + priorityRequests.size();
    }

    public int normalRequestCount() {
        return pendingNormalRequests.size() + normalRequests.size();
    }

    public boolean isPriorityQueueFull() { return priorityRequestCount() >= REQUEST_LIMIT; }

    public boolean isNormalQueueFull() { return normalRequestCount() >= REQUEST_LIMIT; }

    public int getStatus() {
        return status;
    }
}
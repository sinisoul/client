package com.jagex;

/**
 * Created by hadyn on 5/22/2015.
 */
public class MediaLoader {

    private FileIndex mediaFileIndex;
    private int frames;
    private byte[][] colorIndices;
    private byte[][] alphaIndices;
    private int[] widths;
    private int[] heights;
    private int[] offsetsX;
    private int[] offsetsY;
    private int width;
    private int height;
    private int[] palette;

    public MediaLoader(FileIndex mediaFileIndex) {
        this.mediaFileIndex = mediaFileIndex;
    }

    public IndexedColorSprite createIndexedColorSprite(int entryId) {
        if(!load(entryId)) {
            return null;
        }
        IndexedColorSprite sprite = new SoftwareIndexedColorSprite(width, height, widths[0], heights[0], colorIndices[0], palette);
        reset();
        return sprite;
    }

    private boolean load(int entryId) {
        byte[] src = mediaFileIndex.get(entryId, 0);
        if(src != null) {
            load(src);
            return true;
        }
        return false;
    }

    private void load(byte[] src) {
        Buffer buffer = new Buffer(src);
        buffer.position(buffer.capacity() - 2);

        frames = buffer.getUInt16();

        colorIndices = new byte[frames][];
        alphaIndices = new byte[frames][];
        widths = new int[frames];
        heights = new int[frames];
        offsetsX = new int[frames];
        offsetsY = new int[frames];


        buffer.position(buffer.capacity() - (8 * frames) - 7);

        width = buffer.getUInt16();
        height = buffer.getUInt16();

        int colors = buffer.getUInt8() + 1;

        for(int frame = 0; frame < frames; frame++) {
            offsetsX[frame] = buffer.getUInt16();
        }

        for(int frame = 0; frame < frames; frame++) {
            offsetsY[frame] = buffer.getUInt16();
        }

        for(int frame = 0; frame < frames; frame++) {
            widths[frame] = buffer.getUInt16();
        }

        for(int frame = 0; frame < frames; frame++) {
            heights[frame] = buffer.getUInt16();
        }

        buffer.position(buffer.capacity() - (8 * frames) - 7 - (colors * 3) + 3);

        palette = new int[colors];
        for(int i = 1; i < colors; i++) {
            palette[i] = buffer.getUInt24();
            if(palette[i] == 0) {
                palette[i] = 1;
            }
        }

        buffer.position(0);

        for(int frame = 0; frame < frames; frame++) {
            int frameWidth = widths[frame];
            int frameHeight = heights[frame];
            int length = frameWidth * frameHeight;

            byte[] colorIndex = colorIndices[frame] = new byte[length];
            byte[] alphaIndex = alphaIndices[frame] = new byte[length];

            boolean bool = false;

            int flags = buffer.getUInt8();

            if((flags & 0x1) != 0) {
                for(int x = 0; x < frameWidth; x++) {
                    for(int y = 0; y < frameHeight; y++) {
                        byte index = colorIndex[x + y * frameWidth] = buffer.getInt8();
                        bool |= index != -1;
                    }
                }

                if((flags & 0x2) != 0) {
                    for(int x = 0; x < frameWidth; x++) {
                        for(int y = 0; y < frameHeight; y++) {
                            byte alpha = alphaIndex[x + y * frameWidth] = buffer.getInt8();
                            bool |= alpha != -1;
                        }
                    }
                }
            } else {
                for(int i = 0; i < length; i++) {
                    colorIndex[i] = buffer.getInt8();
                }

                if((flags & 0x2) != 0) {
                    for(int i = 0; i < length; i++) {
                        byte alpha = alphaIndex[i] = buffer.getInt8();
                        bool |= alpha != -1;
                    }
                }
            }
        }
    }

    private void reset() {
        frames = -1;
        width = -1;
        height = -1;
        colorIndices = null;
        alphaIndices = null;
        offsetsX = null;
        offsetsY = null;
        palette = null;
    }
}

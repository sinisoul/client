package com.jagex;

import java.awt.*;
import java.io.IOException;
import java.net.Socket;

/**
 * @author Sini
 */
public class GameClient extends GameStub {

    public static final int INITIAL_LOADING = 0;
    public static final int LOADING = 5;

    public static final int UPDATE_ERROR = 1000;

    private LoadingStage loadingStage = LoadingStage.CONNECTING_TO_UPDATE_SERVER;

    private int[] tableProgressScaling = new int[] { 4, 4, 1, 2, 6, 4, 2, 49, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

    private int state = INITIAL_LOADING;

    private BufferedFile mainIndexFile;
    private BufferedFile[] indexFiles;
    private BufferedFile metaVolumeIndex;
    private BufferedFile randomFile;
    private Volume metaVolume;

    private FileSystem fileSystem;
    private DiskEntryWorker diskWorker;
    private RemoteEntryWorker remoteWorker;

    private String remoteAddress;
    private int remotePort;

    private SignlinkRequest updateServerSocketRequest;
    private SocketStream updateServerStream;
    private int updateStage;
    private long updateConnectionStartTime;
    private int currentAttempts;
    private int updateConnectionDelay;
    private boolean updateError;

    private FileIndex configFileIndex;
    private FileIndex modelFileIndex;
    private FileIndex mediaFileIndex;
    private FileIndex fontFileIndex;

    private MediaLoader mediaLoader;

    private IndexedColorSprite background;
    private IndexedColorSprite logo;
    private int backgroundId;
    private int logoId;

    public void setState(int state) {
        if (this.state != state) {
            if (state == LOADING) {
               drawTitleBackground();
            }
            this.state = state;
        }
    }

    private void drawTitleBackground() {
        Graphics2d.clear();

        background = mediaLoader.createIndexedColorSprite(backgroundId);

        // Draw the background scaled to the current view width and height, keep the width constant.
        int height = getViewHeight();
        int width = height * 956 / 503;
        background.draw((getViewWidth() - width) / 2, 0, width, height);

        logo = mediaLoader.createIndexedColorSprite(logoId);
        logo.draw(getViewWidth() / 2 - (logo.getIndexWidth() / 2), 18);
    }

    @Override
    public void setup() {
        diskWorker = new DiskEntryWorker(getSignlink());
        remoteWorker = new RemoteEntryWorker();

        // TODO: Figure this out from the applet document base? Eventually?
        // TODO: Determine the address and port from the parameters
        remoteAddress = "local.runescape.com";
        remotePort = 40000;                                                     // TODO: + nodeId

        updateSize();

        try {
            // Initialize the file store dictionary buffered file
            // For the read buffering store up to 10 consecutive blocks
            mainIndexFile = new BufferedFile(getSignlink().getMainFile(), Volume.SECTOR_BLOCK_LENGTH * 10, 0);

            // Initialize the file store index buffered files
            // For the read buffering store up to 1000 consecutive blocks
            FileOnDisk[] files = getSignlink().getIndexFiles();
            indexFiles = new BufferedFile[files.length];
            for(int i = 0; i < files.length; i++) {
                indexFiles[i] = new BufferedFile(files[i], Volume.REFERENCE_BLOCK_LENGTH * 1000, 0);
            }

            metaVolumeIndex = new BufferedFile(getSignlink().getTableFile(), Volume.REFERENCE_BLOCK_LENGTH * 1000, 0);
            metaVolume = new Volume(255, mainIndexFile, metaVolumeIndex, 500000);

            // Initialize the UID buffered file
            randomFile = new BufferedFile(getSignlink().getRandomFile(), 24, 0);
        } catch(IOException ex) {

            // On failure, rollback the creation of all of the file store
            // buffered files to reset the state
            mainIndexFile = null;
            metaVolumeIndex = null;
            metaVolume = null;
            randomFile = null;
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void process() {
        if(state != UPDATE_ERROR) {

            handleUpdating();

            if(fileSystem != null) {
                fileSystem.process();
            }

            if(state == INITIAL_LOADING) {
                load();
            }
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void teardown() {
        try {
            if(mainIndexFile != null) {
                mainIndexFile.close();
            }

            if(metaVolumeIndex != null) {
                metaVolumeIndex.close();
            }

            if(indexFiles != null) {
                for(int i = 0; i < indexFiles.length; i++) {
                    indexFiles[i].close();
                }
            }
        } catch (IOException ex) {
        }
    }

    private void load() {
        if(loadingStage == LoadingStage.CONNECTING_TO_UPDATE_SERVER) {
            if(fileSystem == null) {
                fileSystem = new FileSystem(mainIndexFile, indexFiles, metaVolume, diskWorker, remoteWorker);
            }

            if(fileSystem.isUpdateTableComplete()) {
                configFileIndex = fileSystem.createFileIndex(2, true, false, true);
                modelFileIndex = fileSystem.createFileIndex(7, false, true, true);
                mediaFileIndex = fileSystem.createFileIndex(8, false, true, true);
                fontFileIndex = fileSystem.createFileIndex(13, false, true, true);
                mediaLoader = new MediaLoader(mediaFileIndex);
                loadingStage = LoadingStage.CHECKING_FOR_UPDATES;
            }
        }

        if(loadingStage == LoadingStage.CHECKING_FOR_UPDATES) {
            int progress = 0;
            for(int indexId = 0; indexId < indexFiles.length; indexId++) {
                IndexRequester requester = fileSystem.getRequester(indexId);
                if(requester != null) {
                    progress += requester.getTableProgress() * tableProgressScaling[indexId] / 100;
                } else {
                    progress += tableProgressScaling[indexId];
                }
            }

            loadingStage.updateMessage(progress);

            if(progress >= 100) {
                getTitleScreenAssetIds();
                loadingStage = LoadingStage.LOADING_TITLE_SCREEN;
            }
        }

        if(loadingStage == LoadingStage.LOADING_TITLE_SCREEN) {
            int completed = 0;

            if(mediaFileIndex.loadEntry(backgroundId)) {
                completed++;
            }

            if(mediaFileIndex.loadEntry(logoId)) {
                completed++;
            }

            if(completed >= 2) {
                setState(LOADING);
                loadingStage = LoadingStage.TEMP;
            } else {
                loadingStage.updateMessage(100 * completed / 2);
            }
        }
    }

    @Override
    public void draw() {
        boolean repaint = false;
        if(getRepaint()) {
            setRepaint(false);
            repaint = true;
        }

        if(repaint) {
            fillPadding();
        }

        if(state != UPDATE_ERROR) {
            if (state == INITIAL_LOADING) {
                drawLoadingBar("Runescape is loading...", loadingStage.getMessage(), loadingStage.getPercent(), repaint);
            }

            Graphics graphics = getCanvas().getGraphics();
            if(state != INITIAL_LOADING) {
                BufferedImageProducer imageProducer = getImageProducer();
                imageProducer.draw(graphics, 0, 0);
            }
        }
    }

    private void getTitleScreenAssetIds() {
        backgroundId = mediaFileIndex.getId("titlebg");
        logoId = mediaFileIndex.getId("logo");
    }

    private void handleUpdating() {
        boolean establishConnection = remoteWorker.process();
        if(establishConnection) {
            establishUpdateConnection();
        }
    }

    private void establishUpdateConnection() {
        if(currentAttempts < remoteWorker.getAttempts()) {

            // Set the update connection delay to wait 5 seconds for every failed attempt
            // greater than 1.
            updateConnectionDelay = 5 * 50 * (remoteWorker.getAttempts() - 1);

            // Limit the next connection delay to be at maximum 60 seconds
            updateConnectionDelay = Math.min(updateConnectionDelay, 3000);

            // If the client build is out of date report it
            if(remoteWorker.getAttempts() >= 2 && remoteWorker.getStatus() == Handshake.STATUS_OUT_OF_DATE) {
                reportUpdateError("js5connect_outofdate");
                state = UPDATE_ERROR;
                return;
            }

            if(remoteWorker.getAttempts() >= 4 && (state == INITIAL_LOADING)) {
                if(remoteWorker.getStatus() > 0) {
                    reportUpdateError("js5connect");
                } else {
                    reportUpdateError("js5io");
                }
                state = UPDATE_ERROR;
                return;
            }
        }

        currentAttempts = remoteWorker.getAttempts();

        if(updateConnectionDelay > 0) {
            updateConnectionDelay--;
        } else {
            try {

                // Create the update server socket
                if (updateStage == 0) {
                    updateServerSocketRequest = getSignlink().createSocket(remoteAddress, remotePort);
                    updateStage++;
                }

                // Await for the update server socket request to finish
                if (updateStage == 1) {

                    // Check if the update server socket request errored
                    // if it did report the error and move along
                    if (updateServerSocketRequest.errored()) {
                        reportUpdateError(RemoteEntryWorker.ERROR_TIMEOUT);
                        return;
                    }

                    // If the request finished increment the stage and move along
                    if (updateServerSocketRequest.isComplete()) {
                        updateStage++;
                    }
                }

                // Write the update server handshake
                if (updateStage == 2) {

                    // Create the update server socket stream
                    updateServerStream = new SocketStream((Socket) updateServerSocketRequest.getResult(), getSignlink(), 5000);

                    // Write the update server handshake to the server
                    // 0: int8  -> update_server_hs
                    // 1: int32 -> build
                    Buffer buffer = new Buffer(5);
                    buffer.putInt8(Handshake.OP_UPDATE_SERVER);
                    buffer.putInt32(getBuild());

                    updateServerStream.write(buffer.array(), 0, buffer.position());
                    updateConnectionStartTime = Time.currentTimeMillis();

                    // Increment the update server connection stage
                    updateStage++;
                }

                // Await for the update server handshake response
                if (updateStage == 3) {

                    // If we are not initially loading, and the stream has no available bytes wait
                    // to read the status since we don't want to block the client. However after
                    // thirty seconds from the time the handshake was written we assume that
                    // the connection timed out.
                    if (state != INITIAL_LOADING && updateServerStream.available() < 1) {
                        if (Time.currentTimeMillis() - updateConnectionStartTime > 30000L) {
                            reportUpdateError(RemoteEntryWorker.ERROR_TIMEOUT);
                            return;
                        }
                    } else {

                        // Read the status from the server, if it is anything
                        // but okay report it as an error.
                        int status = updateServerStream.read();
                        if (status != Handshake.STATUS_OKAY) {
                            reportUpdateError(status);
                            return;
                        }

                        // Increment the update server connection stage
                        updateStage++;
                    }
                }

                // Initialize the remote entry worker
                if (updateStage == 4) {
                    boolean online = false;                                             // TODO: Eventually fix this
                    remoteWorker.setupConnection(updateServerStream, online);
                    updateServerSocketRequest = null;
                    updateStage = 0;
                }
            } catch (IOException ex) {
                reportUpdateError(RemoteEntryWorker.ERROR_IO);
            }
        }
    }

    private void reportUpdateError(int status) {
        remoteWorker.incrementAttempts();
        remoteWorker.setStatus(status);
        updateServerStream = null;
        updateStage = 0;
    }

    private void reportUpdateError(String error) {
        if(!updateError) {
            System.out.println("error_game_" + error);
            updateError = true;
        }
    }

    public static void main(String... args) {
        GameClient client = new GameClient();
        client.launch(Mode.DEVELOPMENT, "runescape", 1, 1024, 768, 28);
    }
}

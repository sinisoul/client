package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class Queue<T extends SubNode> {

    private SubNode head = new SubNode();
    private SubNode iterator;

    public Queue() {
        head.prevSubNode = head;
        head.nextSubNode = head;
    }

    public void add(T node) {
        if(node.prevSubNode != null) {
            node.unlinkSubNode();
        }
        node.prevSubNode = head.prevSubNode;
        node.nextSubNode = head;
        node.prevSubNode.nextSubNode = node;
        node.nextSubNode.prevSubNode = node;
    }

    public T first() {
        SubNode node = head.nextSubNode;
        if(node == head) {
            iterator = null;
            return null;
        }
        iterator = node.nextSubNode;
        return (T) node;
    }

    public T next() {
        SubNode node = iterator;
        if(node == head) {
            iterator = null;
            return null;
        }
        iterator = node.nextSubNode;
        return (T) node;
    }

    public T poll() {
        SubNode node = head.nextSubNode;
        if(node == head) {
            return null;
        }
        node.unlinkSubNode();
        return (T) node;
    }

    public int size() {
        int size = 0;
        for(SubNode node = head.nextSubNode; node != head; node = node.nextSubNode) {
            size++;
        }
        return size;
    }
}

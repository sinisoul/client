package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class Node {
    Node nextNode;
    Node prevNode;
    long hash;

    public void unlinkNode() {
        if(prevNode != null) {
            prevNode.nextNode = nextNode;
            nextNode.prevNode = prevNode;
            prevNode = null;
            nextNode = null;
        }
    }
}

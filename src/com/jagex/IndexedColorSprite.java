package com.jagex;

/**
 * Created by hadyn on 5/22/2015.
 */
public abstract class IndexedColorSprite {
    protected int width;
    protected int height;
    protected int indexWidth;
    protected int indexHeight;
    protected int offsetX;
    protected int offsetY;

    public abstract void draw(int x, int y);
    public abstract void draw(int x, int y, int width, int height);

    public int getIndexWidth() {
        return indexWidth;
    }
}

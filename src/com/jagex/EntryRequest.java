package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public abstract class EntryRequest extends SubNode {

    private byte[] bytes;
    private boolean priority;
    private boolean complete;

    public EntryRequest() {}

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    public boolean isPriority() {
        return priority;
    }

    public void setComplete(boolean complete) {
        synchronized (this) {
            this.complete = complete;
        }
    }

    public boolean isComplete() {
        synchronized (this) {
            return complete;
        }
    }

    public abstract int getProgress();

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public boolean isDiskEntryRequest() {
        return this instanceof DiskEntryRequest;
    }
}

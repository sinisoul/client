package com.jagex;

/**
 * Created by hadyn on 5/22/2015.
 */
public class Graphics2d {

    private static int[] buffer;
    private static int width;
    private static int height;
    private static int clipLowerX;
    private static int clipLowerY;
    private static int clipUpperX;
    private static int clipUpperY;

    public static void setBuffer(int[] buf, int bufWidth, int bufHeight) {
        buffer = buf;
        width = bufWidth;
        height = bufHeight;
        setClip(0, 0, bufWidth, bufHeight);
    }

    public static void setClip(int lowerX, int lowerY, int upperX, int upperY) {
        if(lowerX < 0) {
            lowerX = 0;
        }
        if(lowerY < 0) {
            lowerY = 0;
        }
        if(upperX > width) {
            upperX = width;
        }
        if(upperY > height) {
            upperY = height;
        }
        clipLowerX = lowerX;
        clipLowerY = lowerY;
        clipUpperX = upperX;
        clipUpperY = upperY;
    }

    public static void setClip(int[] clip) {
        clipLowerX = clip[0];
        clipLowerY = clip[1];
        clipUpperX = clip[2];
        clipUpperY = clip[3];
        // TODO: There was a method I skipped over here, involved resetting somethin.
    }

    public static void getClip(int[] clip) {
        clip[0] = clipLowerX;
        clip[1] = clipLowerY;
        clip[2] = clipUpperX;
        clip[3] = clipUpperY;
    }

    public static int getWidth() {
        return width;
    }

    public static int getClipLowerY() {
        return clipLowerY;
    }

    public static int getClipUpperY() {
        return clipUpperY;
    }

    public static int getClipLowerX() {
        return clipLowerX;
    }

    public static int getClipUpperX() {
        return clipUpperX;
    }

    public static int[] getBuffer() {
        return buffer;
    }

    public void resetClip() {
        clipLowerX = 0;
        clipLowerY = 0;
        clipUpperX = width;
        clipUpperY = height;
        // TODO: Method I skipped here, heh
    }

    public static void set(int x, int y, int color) {
        if (x >= clipLowerX && y >= clipLowerY && x < clipUpperX && y < clipUpperY)
            buffer[x + y * width] = color;
    }

    public static void drawVerticalLine(int x, int y, int length, int color) {
        if (x >= clipLowerX && x < clipUpperX) {
            if (y < clipLowerY) {
                length -= clipLowerY - y;
                y = clipLowerY;
            }
            if (y + length > clipUpperY)
                length = clipUpperY - y;
            int off = x + y * width;
            for (int inc = 0; inc < length; inc++)
                buffer[off + inc * width] = color;
        }
    }

    public static void fillRect(int x, int y, int width, int height, int color) {
        if (x < clipLowerX) {
            width -= clipLowerX - x;
            x = clipLowerX;
        }
        if (y < clipLowerY) {
            height -= clipLowerY - y;
            y = clipLowerY;
        }
        if (x + width > clipUpperX)
            width = clipUpperX - x;
        if (y + height > clipUpperY)
            height = clipUpperY - y;
        int step = height * width;                                                      // TODO: This is broken and needs fixing
        int pos = x + y * width;
        for (int i = -height; i < 0; i++) {
            for (int j = -width; j < 0; j++)
                buffer[pos++] = color;
            pos += step;
        }
    }

    public static void clear() {
        for(int i = 0; i < width * height; i++) {
            buffer[i] = 0;
        }
    }
}


package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public abstract class AbstractIndexRequester {

    public static final int READ_FROM_DISK = 0;
    public static final int REMOTE_REQUEST_PRIORITY = 1;
    public static final int REMOTE_REQUEST = 2;

    public abstract int getProgress(int entryId);
    public abstract byte[] get(int type, int entryId);
    public abstract VolumeMetaTable getReferenceTable();

    public abstract int getTableProgress();
}

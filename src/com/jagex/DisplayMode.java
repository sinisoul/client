package com.jagex;

/**
 * Created by hadyn on 5/17/2015.
 */
public class DisplayMode {
    public static final int FIXED = 0;
    public static final int RESIZABLE = 1;
    public static final int FULL_SCREEN = 2;
}

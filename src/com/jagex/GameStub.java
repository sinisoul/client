package com.jagex;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * @author Sini
 */
public abstract class GameStub extends Applet implements Runnable, FocusListener, WindowListener {

    private Signlink signlink;

    private Frame frame;
    private Canvas canvas;

    private int displayMode = DisplayMode.FIXED;

    private int windowWidth;
    private int windowHeight;
    private int viewWidth;
    private int viewHeight;
    private int viewPositionX = 0;
    private int viewPositionY = 0;

    private BufferedImageProducer imageProducer;

    private int mode;
    private int build;

    private Timer timer = new Timer();
    private int delta = 1000 / 50;
    private int min = 1000 / 200;

    private long[] memory = new long[32];
    private int memoryOffset = 0;
    private int currentFps;

    private long destroyTime;

    private Font helveticaFont;
    private FontMetrics helveticaFontMetrics;
    private Image loadingImage;

    private boolean repaint;

    private boolean shutdown;

    /**
     * Launches the game as a window.
     *
     * @param mode The client mode. {@see Mode}
     * @param game The game name.
     * @param build The client build.
     * @param width The window width.
     * @param height The window height.
     * @param indices The amount of cache volumes.
     */
    public void launch(int mode, String game, int build, int width, int height, int indices) {
        this.mode = mode;
        this.build = build;
        this.windowWidth = viewWidth = width;
        this.windowHeight = viewHeight = height;

        frame = new Frame("Jagex");

        Insets insets = frame.getInsets();
        frame.setSize(insets.right + insets.left + windowWidth, insets.top + insets.bottom + windowHeight);

        frame.setLocation(40, 40);
        frame.setResizable(true);

        frame.addWindowListener(this);

        frame.setVisible(true);
        frame.toFront();

        signlink = new Signlink(game, 32 + mode, indices);

        SignlinkRequest request = signlink.createThread(this, 1);
        while(!request.isComplete()) {
            ThreadUtil.sleep(10L);
        }
    }

    /**
     * Gets the signlink.
     *
     * @return The signlink.
     */
    public Signlink getSignlink() {
        return signlink;
    }

    /**
     * Gets the game build.
     *
     * @return The build.
     */
    public int getBuild() {
        return build;
    }

    /**
     * Gets the image producer for the game window.
     *
     * @return The image producer.
     */
    public BufferedImageProducer getImageProducer() {
        return imageProducer;
    }

    /**
     * Gets the canvas for the game window.
     *
     * @return The canvas.
     */
    public Canvas getCanvas() {
        return canvas;
    }

    /**
     * Sets the repaint flag.
     *
     * @param repaint The repaint flag.
     */
    public void setRepaint(boolean repaint) {
        this.repaint = repaint;
    }

    /**
     * Gets the repaint flag.
     *
     * @return The repaint flag.
     */
    public boolean getRepaint() {
        return repaint;
    }

    /**
     * Gets the window width.
     *
     * @return The window width.
     */
    public int getWindowWidth() { return windowWidth; }

    /**
     * Gets the window height.
     *
     * @return The window height.
     */
    public int getWindowHeight() { return windowHeight; }


    /**
     * Gets the view width.
     *
     * @return The view width.
     */
    public int getViewWidth() {
        return viewWidth;
    }

    /**
     * Gets the view height.
     *
     * @return The view height.
     */
    public int getViewHeight() {
        return viewHeight;
    }

    /**
     * Initializes the game client.
     */
    public abstract void setup();

    /**
     * Destroys the game client.
     */
    public abstract void teardown();

    /**
     * Handles the game client logic.
     */
    public abstract void process();

    /**
     * Draws the game.
     */
    public abstract void draw();

    /**
     * Draws the game and calculates the current frames per second.
     */
    private void handleDraw() {
        long curr = Time.currentTimeMillis();
        long val = memory[memoryOffset];
        memory[memoryOffset] = curr;
        memoryOffset = memoryOffset + 1 & 0x1f;
        if (val != -1L && val < curr) {
            int elapsed = (int) (curr - val);
            currentFps = ((elapsed >> 1) + memory.length * 1000) / elapsed;
        }
        draw();
    }

    /**
     * Brings the game window into focus.
     */
    public void focus() {
        if(canvas != null) {
            canvas.removeFocusListener(this);
            canvas.getParent().remove(this);
        }
        Container container;
        if(frame != null) {
            container = frame;
        } else {
            container = this;
        }
        container.setLayout(null);
        canvas = new ComponentDelegator(this);
        container.add(canvas);
        canvas.setSize(viewWidth, viewHeight);
        canvas.setVisible(true);
        if(frame == container) {
            Insets insets = frame.getInsets();
            canvas.setLocation(viewPositionX + insets.left, viewPositionY + insets.top);
        } else {
            canvas.setLocation(viewPositionX, viewPositionY);
        }
        canvas.addFocusListener(this);
        canvas.requestFocus();
    }

    /**
     * Updates the size of the game window.
     */
    public void updateSize() {
        Container container;
        if(frame != null) {
            container = frame;
        } else {
            container = this;
        }
        windowWidth = container.getSize().width;
        windowHeight = container.getSize().height;
        if (frame == container) {
            Insets insets = frame.getInsets();
            windowHeight -= insets.top + insets.bottom;
            windowWidth -= insets.left + insets.right;
        }
        if (displayMode >= DisplayMode.RESIZABLE) {
            viewWidth = windowWidth;
            viewHeight = windowHeight;
            viewPositionX = 0;
            viewPositionY = 0;
        } else {
            viewWidth = 765;
            viewHeight = 503;

            // Center the view in the center of the window for a fixed screen which is
            // defined to be 765 pixels by 503 pixels
            viewPositionX = (windowWidth - 765) / 2;
            viewPositionY = 0;
        }
        canvas.setSize(viewWidth, viewHeight);
        if (frame == container) {
            Insets insets = frame.getInsets();
            canvas.setLocation(viewPositionX + insets.left, insets.top + viewPositionY);
        } else {
            canvas.setLocation(viewPositionX, viewPositionY);
        }
        fillPadding();
    }

    /**
     * Fills the padding between the view and the edges of the game window.
     */
    public void fillPadding() {
        int posX = viewPositionX;
        int posY = viewPositionY;
        int paddingHeight = (windowHeight - viewHeight) - posY;
        int paddingWidth = (viewWidth + windowWidth) - posX;
        if (posX > 0 || paddingWidth > 0 || posY > 0 || paddingHeight > 0) {
            Container container;
            if (frame != null) {
                container = frame;
            } else {
                container = null;
            }
            int topInset = 0;
            int leftInset = 0;
            if (frame == container) {
                Insets insets = frame.getInsets();
                leftInset = insets.left;
                topInset = insets.top;
            }
            Graphics graphics = container.getGraphics();
            graphics.setColor(Color.black);
            if (posX > 0) {
                graphics.fillRect(leftInset, topInset, posX, windowHeight);
            }
            if (posY > 0) {
                graphics.fillRect(leftInset, topInset, windowWidth, posY);
            }
            if (paddingWidth > 0) {
                graphics.fillRect(leftInset + windowWidth - paddingWidth, topInset, paddingWidth, windowHeight);
            }
            if (paddingHeight > 0) {
                graphics.fillRect(leftInset, topInset + windowHeight - paddingHeight, windowWidth, paddingHeight);
            }
        }
    }

    /**
     * Draws the loading bar.
     *
     * @param title The loading bar title.
     * @param text The loading bar text.
     * @param percent The loading percent.
     * @param clear Flag for if the background should be cleared before drawing.
     */
    public void drawLoadingBar(String title, String text, int percent, boolean clear) {
        drawLoadingBar(title, text, clear, percent, new Color(140, 17, 17));
    }

    /**
     * Draws the loading bar.
     *
     * @param title The loading bar title.
     * @param text The loading bar text.
     * @param clear Flag for if the background should be cleared before drawing.
     * @param percent The loading percent.
     * @param color The loading bar color.
     */
    public void drawLoadingBar(String title, String text, boolean clear, int percent, Color color) {
        Graphics canvasGraphics = canvas.getGraphics();

        // Lazily load in the loading bar font and metrics for the canvas
        if(helveticaFont == null) {
            helveticaFont = new Font("Helvetica", 1, 13);
            helveticaFontMetrics = canvas.getFontMetrics(helveticaFont);
        }

        // Clear the canvas with black if needed
        if(clear) {
            canvasGraphics.setColor(Color.black);
            canvasGraphics.fillRect(0, 0, viewWidth, viewHeight);
        }

        // Lazily load in the loading bar image
        if(loadingImage == null) {
            loadingImage = canvas.createVolatileImage(304, 34);
        }

        Graphics graphics = loadingImage.getGraphics();

        // Draw the outline
        graphics.setColor(color);
        graphics.drawRect(0, 0, 303, 33);

        // Draw the inner rectangle
        graphics.fillRect(2, 2, 3 * percent, 30);

        // Draw over the white padding left between the outline and inner rectangle
        graphics.setColor(Color.black);
        graphics.drawRect(1, 1, 301, 31);

        // Draw the absence of the loading bar to fill over the draw space
        graphics.fillRect(3 * percent + 2, 2, 300 - (3 * percent), 30);

        // Draw the loading bar text over the loading bar.
        graphics.setColor(Color.white);
        graphics.setFont(helveticaFont);
        graphics.drawString(text, (304 - helveticaFontMetrics.stringWidth(text)) / 2, 22);

        // Draw the loading bar on the canvas
        canvasGraphics.drawImage(loadingImage, viewWidth / 2 - 152, viewHeight / 2 - 18, null);

        // Draw the loading bar title
        canvasGraphics.setFont(helveticaFont);
        canvasGraphics.setColor(Color.white);
        canvasGraphics.drawString(title, (viewWidth / 2 - (helveticaFontMetrics.stringWidth(title) / 2)), viewHeight / 2 - 26);
    }

    @Override
    public void run() {
        focus();
        setup();
        imageProducer = BufferedImageProducer.create(canvas, viewWidth, viewHeight);
        while(destroyTime == 0L || Time.currentTimeMillis() < destroyTime) {
            int i = timer.sleep(min, delta);
            for(int j = 0; j < i; j++) {
                process();
            }
            handleDraw();
        }
        shutdown(true);
    }

    /**
     * Shuts the game client down.
     *
     * @param clean The clean flag.
     */
    public void shutdown(boolean clean) {
        synchronized (this) {
            if(shutdown) {
                return;
            }
            shutdown = true;
        }

        teardown();
        System.out.println("shutdown complete - clean: " + clean);
    }

    @Override
    public void repaint() {
        repaint = true;
    }

    @Override
    public void windowOpened(WindowEvent e) {}

    @Override
    public void windowClosed(WindowEvent e) {}

    @Override
    public void windowIconified(WindowEvent e) {}

    @Override
    public void windowDeiconified(WindowEvent e) {}

    @Override
    public void windowActivated(WindowEvent e) {}

    @Override
    public void windowDeactivated(WindowEvent e) {}

    @Override
    public void windowClosing(WindowEvent e) {
        destroy();
    }

    @Override
    public void destroy() {
        destroyTime = Time.currentTimeMillis();
        ThreadUtil.sleep(5000L);
        shutdown(false);
    }

    @Override
    public void focusLost(FocusEvent e) {}

    @Override
    public void focusGained(FocusEvent e) {
        repaint = true;
    }
}

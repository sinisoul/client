package com.jagex;

import java.nio.ByteBuffer;

/**
 * Created by hadyn on 5/19/2015.
 */
public class DirectByteBuffer extends AbstractByteArray {

    private ByteBuffer buffer;

    @Override
    public void put(byte[] bytes) {
        buffer = ByteBuffer.allocateDirect(bytes.length);
        buffer.position(0);
        buffer.put(bytes);
    }

    @Override
    public byte[] get() {
        byte[] bytes = new byte[buffer.capacity()];
        buffer.position(0);
        buffer.get(bytes);
        return bytes;
    }

    public static DirectByteBuffer create(byte[] bytes) {
        DirectByteBuffer buf = new DirectByteBuffer();
        buf.put(bytes);
        return buf;
    }
}

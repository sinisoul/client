package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class RemoteEntryRequest extends EntryRequest {

    private Buffer buffer;
    private int indexId;
    private int entryId;
    private int trailerSize;
    private int position;

    public RemoteEntryRequest(int indexId, int entryId, int trailerSize) {
        this.indexId = indexId;
        this.entryId = entryId;
        this.trailerSize = trailerSize;
    }

    public int getIndexId() { return indexId; }
    public int getEntryId() { return entryId; }

    public void setBuffer(Buffer buffer) {
        this.buffer = buffer;
    }

    public int getLength() {
        return buffer.capacity();
    }

    public Buffer getBuffer() {
        return buffer;
    }

    public void position(int position) {
        this.position = position;
    }

    public int position() {
        return position;
    }

    public int getTrailerSize() {
        return trailerSize;
    }

    @Override
    public int getProgress() {
        return buffer == null ? 0 : buffer.position() * 100 / (buffer.capacity() - trailerSize);
    }

    @Override
    public byte[] getBytes() {
        // TODO: Client throws runtime exception here if its not completed :^)
        return buffer.array();
    }
}


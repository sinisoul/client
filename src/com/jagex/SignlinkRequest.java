package com.jagex;

/**
 * Created by hadyn on 5/17/2015.
 */
public class SignlinkRequest {

    public static final int CREATE_THREAD = 0;
    public static final int CREATE_SOCKET = 1;

    public static final int STATUS_INCOMPLETE = 0;
    public static final int STATUS_DONE = 1;
    public static final int STATUS_ERROR = 2;

    private int type;
    private int intArgument;
    private Object objArgument;
    private SignlinkRequest next;
    private Object result;
    private int status = STATUS_INCOMPLETE;

    public SignlinkRequest(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setIntegerArgument(int arg) {
        intArgument = arg;
    }

    public int getIntegerArgument() {
        return intArgument;
    }

    public void setObjectArgument(Object arg) {
        objArgument = arg;
    }

    public Object getObjectArgument() {
        return objArgument;
    }

    public void setNext(SignlinkRequest next) {
        this.next = next;
    }

    public SignlinkRequest getNext() {
        return next;
    }

    public void setResult(Object obj) {
        result = obj;
    }

    public Object getResult() {
        return result;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public boolean isComplete() {
        return status != STATUS_INCOMPLETE;
    }

    public boolean errored() {
        return status == STATUS_ERROR;
    }
}

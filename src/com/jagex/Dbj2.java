package com.jagex;

/**
 * Created by hadyn on 5/22/2015.
 */
public class Dbj2 {

    private Dbj2() {}

    public static int hashOf(String str) {
        int hash = 0;
        for (int i = 0; i < str.length(); i++) {
            hash = str.charAt(i) + ((hash << 5) - hash);
        }
        return hash;
    }
}

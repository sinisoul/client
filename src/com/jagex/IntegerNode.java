package com.jagex;

/**
 * Created by hadyn on 5/21/2015.
 */
public class IntegerNode extends Node {
    private final int value;

    public IntegerNode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

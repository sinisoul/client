package com.jagex;

/**
 * Created by hadyn on 5/17/2015.
 */
public class Mode {
    public static final int PRODUCTION = 0;
    public static final int DEVELOPMENT = 1;
    public static final int TESTING = 2;
}

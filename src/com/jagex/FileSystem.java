package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class FileSystem {
    private IndexRequester[] requesters;

    private BufferedFile mainFile;
    private BufferedFile[] indexFiles;
    private Volume metaVolume;
    private DiskEntryWorker diskWorker;
    private RemoteEntryWorker remoteWorker;

    private EntryRequest updateTableRequest;
    private Buffer updateTableBuffer;

    public FileSystem(BufferedFile mainFile, BufferedFile[] indexFiles, Volume metaVolume, DiskEntryWorker diskWorker, RemoteEntryWorker remoteWorker) {
        this.mainFile = mainFile;
        this.indexFiles = indexFiles;
        this.metaVolume = metaVolume;
        this.diskWorker = diskWorker;
        this.remoteWorker = remoteWorker;

        // If the priority queue is full for the <> then we can
        // wait to request the update table (index 255, entry 255)
        if(remoteWorker.isPriorityQueueFull()) {
            return;
        }

        // Request the update table (index 255, entry 255) so that
        // we can calculate which index table entries (index 255)
        // we need to fetch before we move forward with the next
        // stage of updating
        updateTableRequest = remoteWorker.request(255, 255, true);
    }

    public boolean isUpdateTableComplete() {

        // Check to see if we have captured the update table
        if(updateTableBuffer == null) {

            // Check to see if we have requested the update table,
            // if we haven't only request it if we have availability
            // for it to be priority requested
            if(updateTableRequest == null) {
                if(remoteWorker.isPriorityQueueFull()) {
                    return false;
                }
                updateTableRequest = remoteWorker.request(255, 255, true);
            }

            // If the request has not finished yet then we can't continue further
            if(!updateTableRequest.isComplete()) {
                return false;
            }

            updateTableBuffer = new Buffer(updateTableRequest.getBytes());
            requesters = new IndexRequester[(updateTableBuffer.capacity() - 5) / 8];
        }

        return true;
    }

    public IndexRequester getRequester(int id) {
        return requesters[id];
    }

    public FileIndex createFileIndex(int id, boolean directBufferEntries, boolean directBufferChildren, boolean passivelyLoad) {
        Volume volume = new Volume(id, mainFile, indexFiles[id], -1);
        return new FileIndex(createRequester(id, volume, metaVolume, passivelyLoad), directBufferEntries, directBufferChildren);
    }

    private IndexRequester createRequester(int indexId, Volume volume, Volume tableVolume, boolean passivelyLoad) {
        if(updateTableBuffer == null) {
            throw new IllegalStateException();
        }

        if(requesters[indexId] != null) {
            return requesters[indexId];
        }

        updateTableBuffer.position(5 + indexId * 8);

        if(updateTableBuffer.position() >= updateTableBuffer.capacity()) {
            throw new RuntimeException();
        }

        int checksum = updateTableBuffer.getInt32();
        int revision = updateTableBuffer.getInt32();

        IndexRequester requester = requesters[indexId] = new IndexRequester(indexId, volume, tableVolume, remoteWorker, diskWorker, checksum, revision);
        if(passivelyLoad) {
            requester.loadAll();
        }
        return requester;
    }

    public void process() {
        if(requesters != null) {
            for(int i = 0; i < requesters.length; i++) {
                if(requesters[i] != null) {
                    requesters[i].process();
                }
            }
        }
    }
}

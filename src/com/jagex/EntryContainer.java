package com.jagex;

import com.jagex.bzip.HBZip2InputStream;
import com.jagex.bzip.HBZip2OutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by hadyn on 4/28/15.
 */
public class EntryContainer {
    public static final int COMPRESSION_NONE = 0;
    public static final int COMPRESSION_BZIP2 = 1;
    public static final int COMPRESSION_GZIP = 2;

    private int compression;
    private byte[] bytes;
    private int revision = -1;

    public EntryContainer() {}

    public void setCompression(int compression) {
        this.compression = compression;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public int crc() {
        // TODO
        return -1;
    }

    public int length() {
        return bytes.length;
    }


    public byte[] getBytes() {
        return bytes;
    }

    public int getRevision() {
        return revision;
    }

    public byte[] pack() throws IOException {

        // Get the packed container bytes and calculate
        // the length of the container header
        int headerLength = 5;
        byte[] packed = null;
        if(compression != COMPRESSION_NONE) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // Pack the container data using the specified compression algorithm
            if(compression == COMPRESSION_BZIP2) {
                GZIPOutputStream os = new GZIPOutputStream(bos);
                os.write(bytes, 0, bytes.length);
            } else if(compression == COMPRESSION_GZIP) {
                HBZip2OutputStream os = new HBZip2OutputStream(bos);
                os.write(bytes, 0, bytes.length);
            } else {
                throw new IOException("Unhandled compression type");
            }

            packed = bos.toByteArray();
            headerLength += 4;
        } else {
            packed = bytes;
        }

        boolean versioned = revision != -1;

        // Pack the container, write the container, unpacked length, if the container is
        // compressed; the packed length, and if the container is versioned the revision.
        Buffer buffer = new Buffer(headerLength + packed.length + (versioned ? 2 : 0));
        buffer.putInt8(compression);
        buffer.putInt32(bytes.length);
        if(compression != COMPRESSION_NONE) {
            buffer.putInt32(packed.length);;
        }
        buffer.put(packed);

        // If the container revision attribute is set then write out the
        // entry revision
        if(versioned) {
            buffer.putInt16(revision);
        }

        return buffer.array();
    }

    public static byte[] unpackData(byte[] bytes) throws IOException {
        EntryContainer container = unpack(bytes);
        return container.getBytes();
    }

    public static EntryContainer unpack(byte[] bytes) throws IOException {
        return unpack(bytes, 0, bytes.length);
    }

    public static EntryContainer unpack(byte[] bytes, int off, int len) throws IOException {

        // TODO: Fix this eventually

        // Parse the container header and get the compression type and packed length
        Buffer buffer = new Buffer(bytes);

        int type = buffer.getInt8();
        int packedLength = buffer.getInt32();

        // Create the container and set the container's compression type
        EntryContainer container = new EntryContainer();
        container.setCompression(type);

        // Decompress the container if required and set the container's bytes
        if(type != COMPRESSION_NONE) {
            int unpackedLength = buffer.getInt32();
            byte[] unpacked = new byte[unpackedLength];

            // Skip over the packed bytes in the container
            buffer.skip(packedLength);

            // Unpack the entry based upon its compression type
            InputStream is;
            if(type == COMPRESSION_GZIP) {
                is = new GZIPInputStream(new ByteArrayInputStream(bytes, 9, packedLength));
            } else if(type == COMPRESSION_BZIP2) {
                is = new HBZip2InputStream(new ByteArrayInputStream(bytes, 9, packedLength));
            } else {
                throw new IOException("Unhandled compression type");
            }

            int offset = 0;
            while(offset < unpackedLength) {
                int read = is.read(unpacked, offset, unpackedLength - offset);
                if(read == -1) {
                    break;
                }
                offset += read;
            }

            container.setBytes(unpacked);
        } else {
            byte[] unpacked = new byte[packedLength];
            buffer.get(unpacked, 0, packedLength);
            container.setBytes(unpacked);
        }

        // If the version is encoded at the footer of the container
        // read it from the buffer and set the container version
        // attribute
        boolean versionEncoded = buffer.remaining() >= 2;
        if(versionEncoded) {
            container.setRevision(buffer.getUInt16());
        }

        return container;
    }
}

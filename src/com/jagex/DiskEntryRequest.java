package com.jagex;


public class DiskEntryRequest extends EntryRequest {

    public static final int WRITE = 0;
    public static final int READ_IMMEDIATELY = 1;
    public static final int READ = 2;

    private Volume volume;
    private int type;
    private int entryId;

    public DiskEntryRequest(int type, Volume volume, int entryId) {
        this.volume = volume;
        this.type = type;
        this.entryId = entryId;
    }

    public int getType() {
        return type;
    }

    public Volume getVolume() {
        return volume;
    }

    public int getEntryId() { return entryId; }

    @Override
    public int getProgress() {
        return isComplete() ? 100 : 0;
    }
}


package com.jagex;

import java.awt.*;

/**
 * Created by hadyn on 5/17/2015.
 */
public class ComponentDelegator extends Canvas {

    private Component component;

    public ComponentDelegator(Component component) {
        this.component = component;
    }

    @Override
    public void update(Graphics g) {
        component.update(g);
    }

    @Override
    public void paint(Graphics g) {
        component.paint(g);
    }
}

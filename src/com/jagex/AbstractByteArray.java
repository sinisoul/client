package com.jagex;

/**
 * Created by hadyn on 5/19/2015.
 */
public abstract class AbstractByteArray {
    public abstract void put(byte[] bytes);
    public abstract byte[] get();

    public byte[] copy() {
        byte[] bytes = get();
        byte[] copy = new byte[bytes.length];
        System.arraycopy(bytes, 0, copy, 0, bytes.length);
        return copy;
    }
}

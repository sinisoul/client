package com.jagex;

/**
 * Created by hadyn on 4/27/15.
 */
public class TableEntry {

    private int id = -1;
    private int version;
    private int crc;
    private int name;
    private int size;
    private int length;

    private LinearHashTable names;

    private ChildEntry[] children;
    private int[] childIds;
    private int revision;

    public TableEntry() {}

    public void parseChildren(Buffer buffer, int position) {

        // Calculate the length of the entry
        int counter = 0, max = -1;
        childIds = new int[size];
        for(int j = 0; j < size; j++) {
            int id = childIds[j] = counter += buffer.getUInt16();
            if(id > max) {
                max = id;
            }
        }
        length = max + 1;

        // Create an initialize each of the children for the entry
        children = new ChildEntry[length];
        for(int i = 0; i < size; i++) {
            ChildEntry entry = children[childIds[i]] = new ChildEntry();
            entry.setId(childIds[i]);

            // If we need to read the name offsets position to where
            // the child name block starts and reposition the buffer back
            // to the previous position
            if(position != -1) {
                int pos = buffer.position();
                buffer.position(position);
                entry.setName(buffer.getInt32());
                buffer.position(pos);
            }
        }

        // Initialize the name hash table if needed
        if(position != -1) {
            int[] arr = new int[size];
            for(int i = 0; i < size; i++) {
                ChildEntry entry = children[childIds[i]];
                arr[i] = entry.getName();
            }
            names = new LinearHashTable(arr);
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setCrc(int crc) {
        this.crc = crc;
    }

    public void setName(int name) {
        this.name = name;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int size() {
        return size;
    }

    public boolean exists(int childId) {
        if(childId < 0 || childId > length) {
            throw new ArrayIndexOutOfBoundsException(childId);
        }
        return children[childId] != null;
    }

    /**
     * Helper method;
     *
     * @param childId
     * @return
     */
    public int getChildName(int childId) {
        if(names == null) {
            return -1;
        }
        return names.get(childId);
    }

    /**
     *
     * @param childId
     * @return
     */
    public ChildEntry getChild(int childId) {
        if(childId < 0 || childId > length) {
            throw new ArrayIndexOutOfBoundsException(childId);
        }
        return children[childId];
    }

    public ChildEntry[] getChildren() {
        return children;
    }

    public int[] getChildIds() {
        return childIds;
    }

    /**
     * Gets the name of the entry.
     *
     * @return
     */
    public int getName() {
        return name;
    }

    public int length() {
        return length;
    }

    public int getChecksum() {
        return crc;
    }

    public int getRevision() {
        return revision;
    }
}

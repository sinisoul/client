package com.jagex;

import java.awt.*;
import java.awt.image.*;
import java.util.Hashtable;

/**
 * Created by hadyn on 5/22/2015.
 */
public class BufferedImageProducer {

    private Component parent;
    private Image image;
    private int[] buffer;
    private int width;
    private int height;

    public void setComponent(Component component, int width, int height) {
        buffer = new int[width * height + 1];
        this.height = height;
        this.width = width;
        DataBufferInt buf = new DataBufferInt(buffer, buffer.length);
        DirectColorModel model = new DirectColorModel(32, 0xff0000, 0xff00, 0xff);
        WritableRaster raster = Raster.createWritableRaster(model.createCompatibleSampleModel(width, height), buf, null);
        image = new BufferedImage(model, raster, false, new Hashtable());
        parent = component;
        initGraphics();
    }

    public void draw(Graphics graphics, int x, int y) {
        graphics.drawImage(image, x, y, parent);
    }

    public void draw(Graphics graphics, int clipX, int clipY, int clipWidth, int clipHeight) {
        Shape clip = graphics.getClip();
        graphics.clipRect(clipX, clipY, clipWidth, clipHeight);
        graphics.setClip(clip);
    }

    public void initGraphics() {
        Graphics2d.setBuffer(buffer, width, height);
    }

    public static BufferedImageProducer create(Component component, int width, int height) {
        BufferedImageProducer producer = new BufferedImageProducer();
        producer.setComponent(component, width, height);
        return producer;
    }
}

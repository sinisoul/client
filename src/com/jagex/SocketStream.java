package com.jagex;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by hadyn on 5/3/2015.
 */
public class SocketStream implements Runnable {

    private Signlink signlink;
    private Socket socket;
    private SignlinkRequest threadRequest;
    private InputStream inputStream;
    private OutputStream outputStream;
    private byte[] buffer;
    private int queuePosition;
    private int writePosition;
    private int capacity;
    private boolean exceptionEncountered;
    private boolean closed;

    public SocketStream(Socket socket, Signlink signlink, int capacity) throws IOException {
        this.socket = socket;
        this.signlink = signlink;
        this.capacity = capacity;
        socket.setSoTimeout(30000);
        socket.setTcpNoDelay(true);
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
    }

    public void write(byte[] bytes, int off, int len) throws IOException{
        if(!closed) {
            if (!exceptionEncountered) {

                // If the buffer has not yet been initialized, initialize it
                if (buffer == null) {
                    buffer = new byte[capacity];
                }

                synchronized (this) {

                    // Write all of the bytes to the queue buffer, if we are approaching the circular
                    // buffer capacity throw an I/O exception
                    for (int i = 0; i < len; i++) {
                        buffer[queuePosition] = bytes[off + i];
                        queuePosition = (queuePosition + 1) % capacity;
                        if (queuePosition == (writePosition + (capacity - 100)) % capacity) {
                            throw new IOException("Buffer overflow");
                        }
                    }

                    // If the thread for the stream hasn't been initalized
                    // request it and move along
                    if (threadRequest == null) {
                        threadRequest = signlink.createThread(this, 3);
                    }

                    notifyAll();
                }
            } else {
                exceptionEncountered = false;
                throw new IOException("Exception encountered in socket stream thread");
            }
        }
    }

    public int read() throws IOException {
        return closed ? 0 : inputStream.read();
    }

    public int available() throws IOException {
        return closed ? 0 : inputStream.available();
    }

    public void read(byte[] bytes, int off, int len) throws IOException {
        if(!closed) {
            while (len > 0) {
                int read = inputStream.read(bytes, off, len);
                if (read <= 0) {
                    throw new EOFException("End of stream");
                }
                off += read;
                len -= read;
            }
        }
    }

    @Override
    public void run() {
        while(true) {
            int off;
            int len;
            synchronized (this) {

                if(closed) {
                    break;
                }

                // If there are not any bytes to be written wait until
                // we are notified of a write.
                if(queuePosition == writePosition) {
                    try {
                        wait();
                    } catch(InterruptedException ex) {
                    }
                }

                // Get the amount of bytes to write out to the socket
                off = writePosition;
                if(queuePosition < writePosition) {
                    len = capacity - writePosition;
                } else {
                    len = queuePosition - writePosition;
                }
            }

            // If we have nothing to write just continue the loop
            if(len <= 0) {
                continue;
            }

            // Write the bytes out to the socket
            try {
                outputStream.write(buffer, off, len);
            } catch(IOException ex) {
                exceptionEncountered = true;
            }

            // Increment the write position
            writePosition = (writePosition + len) % capacity;

            // If we have reached the queued buffer position with the written
            // bytes to the socket flush the output stream
            try {
                if(queuePosition == writePosition) {
                    outputStream.flush();
                }
            } catch (IOException ex) {
                exceptionEncountered = true;
            }
        }

        try {
            inputStream.close();
            outputStream.close();
            socket.close();
        } catch (IOException ex) {

        }

        buffer = null;
    }

    public void close() {
        if(!closed) {
            synchronized (this) {
                closed = true;
                notifyAll();
            }

            while(!threadRequest.isComplete()) {
                ThreadUtil.sleep(10L);
            }

            Thread result = (Thread) threadRequest.getResult();

            try {
                result.join();
            } catch(InterruptedException ex) {

            }

            threadRequest = null;
        }
    }
}

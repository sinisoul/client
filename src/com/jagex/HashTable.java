package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class HashTable<T extends Node> {
    private Node[] nodes;
    private int capacity;
    private Node iterator;
    private long lastKey;

    public HashTable(int capacity) {
        nodes = new Node[capacity];
        this.capacity = capacity;
        for(int i = 0; i < capacity; i++) {
            Node node = nodes[i] = new Node();
            node.nextNode = node;
            node.prevNode = node;
        }
    }

    public void put(long key, T node) {
        Node bucket = nodes[(int) (key & (long) (capacity - 1))];

        // Add the node to the end of the list
        node.nextNode = bucket;
        node.prevNode = bucket.prevNode;
        node.prevNode.nextNode = node;
        node.nextNode.prevNode = node;

        // Set the node hash
        node.hash = key;
    }

    public T get(long key) {
        Node bucket = nodes[(int) (key & (long) (capacity - 1))];
        lastKey = key;

        for(iterator = bucket.nextNode; iterator != bucket; iterator = iterator.nextNode) {
            if(iterator.hash == key) {
                T node = (T) iterator;
                iterator = iterator.nextNode;
                return node;
            }
        }
        iterator = null;
        return null;
    }
}

package com.jagex;

/**
 * Created by hadyn on 5/18/2015.
 */
public class LoadingStage {

    public static final LoadingStage CONNECTING_TO_UPDATE_SERVER = new LoadingStage("Connecting to update server", 5);
    public static final LoadingStage CHECKING_FOR_UPDATES = new LoadingStage("Checking for updates %d%%", 20, 0);
    public static final LoadingStage LOADING_TITLE_SCREEN = new LoadingStage("Loading config %d%%", 30, 0);
    public static final LoadingStage TEMP = new LoadingStage("Temp", 100);

    private String template;
    private String message;
    private int percent;

    private LoadingStage(String template, int percent, Object... defaultArgs) {
        this.template = template;
        this.percent = percent;
        message = String.format(template, defaultArgs);
    }

    public void updateMessage(Object... args) {
        message = String.format(template, args);
    }

    public String getMessage() {
        return message;
    }

    public int getPercent() {
        return percent;
    }
}

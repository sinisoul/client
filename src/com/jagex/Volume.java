package com.jagex;

import java.io.IOException;
import java.nio.BufferUnderflowException;

/**
 * Created by hadyn on 5/18/2015.
 */
public class Volume {
    public static final int REFERENCE_BLOCK_LENGTH = 6;
    public static final int SECTOR_BLOCK_LENGTH = 520;
    public static final int SECTOR_HEADER_LENGTH = 8;
    private static final int ENTRY_END = 0;

    private BufferedFile mainFile;
    private BufferedFile indexFile;
    private byte[] buffer = new byte[SECTOR_BLOCK_LENGTH];
    private int volumeId;

    public Volume(int id, BufferedFile mainFile, BufferedFile indexFile, int maximumLength) {
        this.volumeId = id;
        this.mainFile = mainFile;
        this.indexFile = indexFile;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return volumeId;
    }

    /**
     *
     * @param entryId
     * @return
     * @throws java.io.IOException
     */
    public int length(int entryId) throws IOException {

        /**
         * Check that the entry exists in the volume.
         */
        if(!referenceExists(entryId)) {
            return -1;
        }

        seekToReference(entryId);

        /**
         * Read the entry size from the index block.
         */
        Buffer buf = new Buffer(buffer);
        indexFile.read(buffer, 0, REFERENCE_BLOCK_LENGTH);
        int size = buf.getUInt24();

        return size;
    }

    public byte[] read(int entryId) {
        try {
            int len = length(entryId);
            if (len == -1) {
                return null;
            }
            byte[] dest = new byte[len];
            int read = read(entryId, dest);
            if (read == -1) {
                return null;
            }
            return dest;
        } catch (IOException ex) {
            return null;
        }
    }


    /**
     *
     * @param entryId
     * @param dest
     * @return
     * @throws java.io.IOException
     */
    public int read(int entryId, byte[] dest) throws IOException {
        return read(entryId, dest, 0, dest.length);
    }

    /**
     *
     * @param entryId
     * @param dest
     * @param off
     * @param len
     * @return
     * @throws java.io.IOException
     */
    public int read(int entryId, byte[] dest, int off, int len) throws IOException {
        if(off < 0 || len < 0 || off + len > dest.length) {
            throw new ArrayIndexOutOfBoundsException();
        }

        /**
         * Check that the entry exists in the volume,
         * if it doesn't then we obviously cannot
         * read anything.
         */
        if(!referenceExists(entryId)) {
            return -1;                                                              // TODO: Throw exception?
        }

        seekToReference(entryId);

        /**
         * Read the index block.
         */
        Buffer indexBuf = new Buffer(buffer);
        indexFile.read(buffer, 0, REFERENCE_BLOCK_LENGTH);
        int entryLength = indexBuf.getUInt24();
        int nextSector = indexBuf.getUInt24();

        /**
         * If the amount of bytes to be read is less than the
         * amount available throw an underflow exception.
         */
        if(len > entryLength) {
            throw new BufferUnderflowException();
        }

        int chunkId = 0;
        int pos = 0;
        for(; pos < len;) {

            /**
             * Check if the next sector exists, if it doesn't
             * then we can assume the entry has been corrupted.
             */
            if(!sectorExists(nextSector)) {
                return -1;                                                          // TODO: Throw exception?
            }

            int read = len - pos;
            if(read > 512) {
                read = 512;
            }

            /**
             * Read the next blob block and check that it is
             * valid, if it isn't then we can assume the entry
             * has been corrupted.
             */
            seekToSector(nextSector);

            Buffer buf = new Buffer(buffer);
            mainFile.read(buffer, 0, SECTOR_HEADER_LENGTH + read);

            int entry = buf.getUInt16();
            int chunk = buf.getUInt16();
            nextSector = buf.getUInt24();
            int index = buf.getUInt8();

            if(index != volumeId || entry != entryId || chunk != chunkId) {
                return -1;                                                          // TODO: Throw exception?
            }

            buf.get(dest, off + pos, read);

            pos += read;
            chunkId++;
        }
        return pos;
    }

    /**
     *
     * @param entryId
     * @param bytes
     * @return
     * @throws IOException
     */
    public boolean write(int entryId, byte[] bytes) throws IOException {
        return write(entryId, bytes, 0, bytes.length);
    }

    /**
     *
     * @param entryId
     * @param bytes
     * @param off
     * @param len
     * @return
     * @throws java.io.IOException
     */
    public boolean write(int entryId, byte[] bytes, int off, int len) throws IOException {

        /**
         * First attempt to overwrite the entry if it exists in the volume,
         * if that fails attempt to just write a new entry.
         */
        if(!write(entryId, bytes, off, len, true)) {
            return write(entryId, bytes, off, len, false);
        }

        return true;
    }

    /**
     *
     * @param entryId
     * @param bytes
     * @param off
     * @param len
     * @param overwrite
     * @return
     * @throws java.io.IOException
     */
    private boolean write(int entryId, byte[] bytes, int off, int len, boolean overwrite) throws IOException {
        try {

            seekToReference(entryId);

            int nextSector;
            if (overwrite) {

                /**
                 * We obviously cannot overwrite an entry
                 * that doesn't exist in the volume.
                 */
                if (!referenceExists(entryId)) {
                    return false;
                }

                /**
                 * Read the first block so that we can simply expect to overwrite it.
                 */
                Buffer buf = new Buffer(buffer);
                indexFile.read(buffer, 0, 6);
                buf.skip(3);
                nextSector = buf.getUInt24();
            } else {

                /**
                 * If we aren't overwriting the entry get the next
                 * block id so we can append the data to the volume.
                 */
                nextSector = getNextSector();

                Buffer buf = new Buffer(buffer);
                buf.putInt24(len);
                buf.putInt24(nextSector);

                indexFile.write(buffer, 0, REFERENCE_BLOCK_LENGTH);
            }

            int chunkId = 0, pos = 0, sector = nextSector;
            for(; pos < len;) {
                if(overwrite) {

                    /**
                     * Seek to the next blob block.
                     */
                    seekToSector(sector);

                    if(!sectorExists(sector)) {
                        return false;
                    }

                    Buffer buf = new Buffer(buffer);
                    mainFile.read(buffer, 0, SECTOR_HEADER_LENGTH);

                    /**
                     * Read the blob header to check a few things just to make sure
                     * that the entry hasn't been corrupted.
                     */
                    int entry = buf.getUInt16();
                    int chunk = buf.getUInt16();
                    nextSector = buf.getUInt24();
                    int index = buf.getUInt8();

                    if(index != volumeId || entry != entryId || chunk != chunkId) {
                        return false;
                    }

                    /**
                     * In cases where we are overwriting the file
                     * with a larger file then we can stop overwriting
                     * once we reach the end.
                     */
                    if(sector == ENTRY_END) {                                        // TODO: What do we do here D:
                        nextSector = getNextSector();
                        overwrite = false;
                    }
                } else {
                    nextSector++;
                }

                /**
                 * Calculate how much of the data we will write
                 * for this blob block.
                 */
                int dataLength = len - pos;
                if(dataLength > 512) {
                    dataLength = 512;
                }

                /**
                 * If we are writing the last blob block
                 * then just write the end.
                 */
                if(dataLength < 512) {
                    nextSector = ENTRY_END;
                }


                Buffer buf = new Buffer(buffer);

                /**
                 * Write the block header to the buffer.
                 */

                buf.putInt16(entryId);
                buf.putInt16(chunkId);
                buf.putInt24(nextSector);
                buf.putInt8(volumeId);

                /**
                 * Write the data.
                 */
                buf.put(bytes, off + pos, dataLength);

                /**
                 * Seek to the next blob block,
                 * in cases where we overwrite
                 * we need to do this to properly
                 * align where we are writing
                 */
                seekToSector(sector);

                mainFile.write(buffer, 0, SECTOR_HEADER_LENGTH + dataLength);

                sector = nextSector;
                pos += dataLength;
                chunkId++;
            }

        } catch(IOException ex) {
            return false;
        }

        return true;
    }

    /**
     * Get if an entry exists in the index.
     *
     * @param entryId The entry id.
     * @return If the entry exists.
     * @throws java.io.IOException An I/O exception was encountered while determining if the entry exists.
     */
    private boolean referenceExists(int entryId) throws IOException{
        return ((indexFile.size() + REFERENCE_BLOCK_LENGTH - 1L) / (long) REFERENCE_BLOCK_LENGTH) > entryId;
    }

    /**
     *
     * @param blockId
     * @return
     * @throws java.io.IOException
     */
    private boolean sectorExists(int blockId) throws IOException {
        return ((mainFile.size() + SECTOR_BLOCK_LENGTH - 1L) / (long) SECTOR_BLOCK_LENGTH) > blockId;
    }

    /**
     *
     * @return
     * @throws java.io.IOException
     */
    private int getNextSector() throws IOException {
        int block = (int) ((mainFile.size() + SECTOR_BLOCK_LENGTH - 1L) / (long) SECTOR_BLOCK_LENGTH);
        return block == 0 ? 1 : block;
    }

    /**
     * Seeks the index channel to a specific index block.
     *
     * @param entryId The entry id.
     * @throws java.io.IOException An I/O exception was encountered while seeking.
     */
    private void seekToReference(int entryId) throws IOException {
        indexFile.seek(entryId * (long) REFERENCE_BLOCK_LENGTH);
    }

    /**
     * Seeks the blob channel to a specific blob block.
     *
     * @param blockId The block id.
     * @throws java.io.IOException An I/O exception was encountered while seeking.
     */
    private void seekToSector(int blockId) throws IOException {
        mainFile.seek(blockId * (long) SECTOR_BLOCK_LENGTH);
    }
}

package com.jagex;

/**
 * Created by hadyn on 5/17/2015.
 */
public class Timer {
    private long nextTime = System.nanoTime();

    public int sleep(int minimum, int delta) {
        long min = 1000000L * (long) minimum;
        long diff = nextTime - System.nanoTime();
        if (diff < min) {
            diff = min;
        }
        ThreadUtil.sleep(diff / 1000000L);
        long curr = System.nanoTime();
        int i;
        for (i = 0; i < 10 && (i < 1 || curr > nextTime); nextTime += 1000000L * (long) delta) {
            i++;
        }
        if (nextTime < curr) {
            nextTime = curr;
        }
        return i;
    }

    public void reset() {
        nextTime = System.nanoTime();
    }
}

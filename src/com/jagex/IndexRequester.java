package com.jagex;

import java.util.zip.CRC32;

/**
 * Created by hadyn on 5/20/2015.
 */
public class IndexRequester extends AbstractIndexRequester {

    private static final CRC32 crc = new CRC32();

    private static final int COMPLETED = 1;
    private static final int INCOMPLETE = 0;
    private static final int ERROR = -1;

    private int indexId;
    private Volume volume;
    private Volume tableVolume;
    private RemoteEntryWorker remoteEntryWorker;
    private DiskEntryWorker diskEntryWorker;
    private int expectedChecksum;
    private int expectedRevision;
    private HashTable<EntryRequest> requests = new HashTable(16);
    private Queue pendingRequests = new Queue();
    private boolean requestEntries;
    private boolean requestFromDisk;
    private boolean storeRequests;
    private int entryIndex;
    private VolumeMetaTable table;
    private EntryRequest tableRequest;
    private byte[] states;

    public IndexRequester(int indexId, Volume volume, Volume tableVolume, RemoteEntryWorker remoteEntryWorker, DiskEntryWorker diskEntryWorker, int expectedChecksum, int expectedRevision) {
        this.indexId = indexId;
        this.volume = volume;
        this.tableVolume = tableVolume;
        this.remoteEntryWorker = remoteEntryWorker;
        this.diskEntryWorker = diskEntryWorker;
        this.expectedChecksum = expectedChecksum;
        this.expectedRevision = expectedRevision;
        requestFromDisk = true;                                                         // TODO: If index is null, virtual file system, then this needs to be false
        tableRequest = diskEntryWorker.immediatelyRead(tableVolume, indexId);
    }

    public void loadAll() {
        pendingRequests = new Queue<>();
        requestEntries = true;
    }

    /*
    public void process() {
        if(getReferenceTable() != null) {
            for(SubNode request = pendingRequests.first(); request != null; request = pendingRequests.next()) {
                int entryId = (int) request.hash;
                if(entryId < 0 || entryId >= table.length() || !table.contains(entryId)) {
                    request.unlinkNode();
                    return;
                }

                if(states[entryId] == INCOMPLETE) {
                    request(READ_FROM_DISK, entryId);
                }

                if(states[entryId] == ERROR) {
                    request(REMOTE_REQUEST, entryId);
                }

                if(states[entryId] == ERROR) {
                    request.unlinkNode();
                }
            }
        }
    }
*/
    // TODO: Replace this with an integer node!!!!!!!
    public void process() {
        if(pendingRequests != null) {
            if(getReferenceTable() == null) {
                return;
            }

            if(!requestFromDisk) {
                if(!requestEntries) {
                    pendingRequests = null;
                    return;
                }

                boolean finished = true;
                for(SubNode request = pendingRequests.first(); request != null; request = pendingRequests.next()) {
                    int id = (int) request.hash;

                    if(states[id] != COMPLETED) {
                        request(REMOTE_REQUEST, id);
                    }

                    if(states[id] == COMPLETED) {
                        request.unlinkNode();
                    } else {
                        finished = false;
                    }
                }

                int[] entryIds = table.getEntryIds();
                while(entryIndex < table.size()) {
                    int entryId = entryIds[entryIndex];
                    if(remoteEntryWorker.isNormalQueueFull()) {
                        finished = false;
                        break;
                    }
                    if(states[entryId] != COMPLETED) {
                        request(REMOTE_REQUEST, entryId);
                    }

                    if(states[entryId] != ERROR) {
                        finished = false;
                        SubNode node = new SubNode();
                        node.hash = (long) entryId;
                        pendingRequests.add(node);
                    }

                    entryIndex++;
                }

                if(finished) {
                    requestEntries = false;
                    entryIndex = 0;
                }
            } else {
                boolean finished = true;
                for(SubNode request = pendingRequests.first(); request != null; request = pendingRequests.next()) {
                    int id = (int) request.hash;

                    if(states[id] == INCOMPLETE) {
                        request(READ_FROM_DISK, id);
                    }

                    if(states[id] != INCOMPLETE) {
                        request.unlinkNode();
                    } else {
                        finished = false;
                    }
                }

                int[] entryIds = table.getEntryIds();
                while(entryIndex < table.size()) {
                    int entryId = entryIds[entryIndex];
                    if(diskEntryWorker.size() >= 250) {
                        finished = false;
                        break;
                    }

                    if(states[entryId] == INCOMPLETE) {
                        request(READ_FROM_DISK, entryId);
                    }

                    if(states[entryId] == INCOMPLETE) {
                        finished = false;
                        SubNode node = new SubNode();
                        node.hash = (long) entryId;
                        pendingRequests.add(node);
                    }

                    entryIndex++;
                }

                if(finished) {
                    requestFromDisk = false;
                    entryIndex = 0;
                }
            }
        }
    }

    @Override
    public int getProgress(int entryId) {
        EntryRequest request = requests.get(entryId);
        return request != null ? request.getProgress() : 0;
    }

    public EntryRequest request(int method, int entryId) {
        EntryRequest request = requests.get(entryId);
        if(request != null && method == REMOTE_REQUEST_PRIORITY && !request.isPriority() && !request.isComplete()) {
            request.unlinkNode();
            request = null;
        }

        if(request == null) {
            if(method == REMOTE_REQUEST_PRIORITY) {
                if(states[entryId] != ERROR) {
                    request = diskEntryWorker.immediatelyRead(volume, entryId);
                } else {
                    if(remoteEntryWorker.isPriorityQueueFull()) {
                        return null;
                    }
                    request = remoteEntryWorker.request(indexId, entryId, 2, true);
                }
            } else if(method == REMOTE_REQUEST) {
                if(states[entryId] != ERROR) {
                    throw new IllegalStateException("Expecting entry to be attempted to be read from disk");
                }

                if(remoteEntryWorker.isNormalQueueFull()) {
                    return null;
                }

                request = remoteEntryWorker.request(indexId, entryId, 2, false);
            } else if(method == READ_FROM_DISK) {
                request = diskEntryWorker.read(volume, entryId);
            } else {
                throw new IllegalArgumentException("Unhandled request method");
            }

            // Store the request in the requests table
            requests.put(entryId, request);
        }

        if(!request.isComplete()) {
            return null;
        }

        byte[] bytes = request.getBytes();
        if(request.isDiskEntryRequest()) {
            try {
                if (bytes != null && bytes.length > 2) {
                    TableEntry entry = table.get(entryId);

                    crc.reset();
                    crc.update(bytes, 0, bytes.length - 2);
                    int checksum = (int) crc.getValue();

                    if(checksum != entry.getChecksum()) {
                        throw new RuntimeException("CRC mismatch");
                    }

                    int revision = (bytes[bytes.length - 2] & 0xff) << 8 |
                                   (bytes[bytes.length - 1] & 0xff);

                    if(revision != entry.getRevision()) {
                        throw new RuntimeException("Revision mismatch");
                    }

                    return request;
                } else {
                    throw new RuntimeException("No such entry exists");
                }
            } catch (Exception ex) {
                states[entryId] = ERROR;
                request.unlinkNode();

                if(request.isPriority() && !remoteEntryWorker.isPriorityQueueFull()) {
                    request = remoteEntryWorker.request(indexId, entryId, 2, true);
                    requests.put(entryId, request);
                }

                return null;
            }
        } else {
            TableEntry entry = table.get(entryId);
            try {
                if(bytes == null || bytes.length <= 2) {
                    throw new RuntimeException("Failed to fetch entry");
                }

                crc.reset();
                crc.update(bytes, 0, bytes.length - 2);
                int checksum = (int) crc.getValue();

                if(checksum != entry.getChecksum()) {
                    throw new RuntimeException("CRC mismatch");
                }

                // TODO: Set attempts to 0
                // TODO: Enumerate?
                remoteEntryWorker.setStatus(0);
            } catch (RuntimeException ex) {
                // TODO: Encryption key
                request.unlinkNode();

                if(request.isPriority() && !remoteEntryWorker.isPriorityQueueFull()) {
                    request = remoteEntryWorker.request(indexId, entryId, 2, true);
                    requests.put(entryId, request);
                }

                return null;
            }

            int revision = entry.getRevision();
            bytes[bytes.length - 2] = (byte) (revision >> 8);
            bytes[bytes.length - 1] = (byte) (revision);

            diskEntryWorker.write(volume, entryId, bytes);
            if(states[entryId] != COMPLETED) {
                states[entryId] = COMPLETED;
            }

            if(!request.isPriority()) {
                request.unlinkNode();
            }

            return request;
        }
    }

    @Override
    public byte[] get(int method, int entryId) {
        EntryRequest request = request(method, entryId);
        if(request == null) {
           return null;
        }
        request.unlinkNode();
        return request.getBytes();
    }

    @Override
    public int getTableProgress() {
        return getReferenceTable() != null ? 100 : tableRequest == null ? 0 : tableRequest.getProgress();
    }

    @Override
    public VolumeMetaTable getReferenceTable() {
        if (table != null) {
            return table;
        }

        if (tableRequest == null) {
            if (remoteEntryWorker.isPriorityQueueFull()) {
                return null;
            }
            tableRequest = remoteEntryWorker.request(255, indexId, true);
        }

        if (!tableRequest.isComplete()) {
            return null;
        }

        byte[] bytes = tableRequest.getBytes();
        if (tableRequest.isDiskEntryRequest()) {
            try {
                if (bytes == null) {
                    throw new RuntimeException("Failed to fetch entry");
                }

                table = new VolumeMetaTable(bytes, expectedChecksum);
                if (table.getRevision() != expectedRevision) {
                    throw new RuntimeException("Version mismatch");
                }
            } catch (RuntimeException ex) {
                table = null;
                if(remoteEntryWorker.isPriorityQueueFull()) {
                    return null;
                }
                tableRequest = remoteEntryWorker.request(255, indexId, true);
                return null;
            }
        } else {
            try {
                if(bytes == null) {
                    throw new RuntimeException("Failed to fetch entry");
                }

                table = new VolumeMetaTable(bytes, expectedChecksum);
            } catch (RuntimeException ex) {
                // Encryption key here
                table = null;
                if(remoteEntryWorker.isPriorityQueueFull()) {
                    return null;
                }
                tableRequest = remoteEntryWorker.request(255, indexId, true);
                return null;
            }

            diskEntryWorker.write(tableVolume, indexId, bytes);
        }

        states = new byte[table.length()];
        tableRequest = null;
        return table;
    }
}

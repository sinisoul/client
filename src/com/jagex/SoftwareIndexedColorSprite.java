package com.jagex;

/**
 * Created by hadyn on 5/22/2015.
 */
public class SoftwareIndexedColorSprite extends IndexedColorSprite {

    protected byte[] index;
    protected int[] palette;

    public SoftwareIndexedColorSprite(int width, int height, int indexWidth, int indexHeight, byte[] index, int[] palette) {
        this.width = width;
        this.height = height;
        this.indexWidth = indexWidth;
        this.indexHeight = indexHeight;
        this.index = index;
        this.palette = palette;
    }

    public SoftwareIndexedColorSprite(int width, int height, int paletteSize) {
        offsetX = offsetY = 0;
        this.width = indexWidth = width;
        this.height = indexHeight = height;
        palette = new int[paletteSize];
    }

    @Override
    public void draw(int x, int y) {
        x += offsetX;
        y += offsetY;

        int width = indexWidth;
        int height = indexHeight;

        int srcOff = 0;
        int destOff = x + y * Graphics2d.getWidth();

        int srcStep = 0;
        int destStep = Graphics2d.getWidth() - width;

        int widthDiff;
        if(x < Graphics2d.getClipLowerX()) {
            widthDiff = Graphics2d.getClipLowerX() - x;
            width -= widthDiff;
            x = Graphics2d.getClipLowerX();
            srcOff += widthDiff;
            destOff += widthDiff;
            srcStep += widthDiff;
            destStep += widthDiff;
        }

        if(x + width > Graphics2d.getClipUpperX()) {
            widthDiff = x + width - Graphics2d.getClipUpperX();
            width -= widthDiff;
            srcStep += widthDiff;
            destStep += widthDiff;
        }

        int heightDiff;
        if(y < Graphics2d.getClipLowerY()) {
            heightDiff = Graphics2d.getClipLowerY() - y;
            height -= heightDiff;
            y = Graphics2d.getClipLowerY();
            srcOff += heightDiff * width;
            destOff += heightDiff * Graphics2d.getWidth();
        }

        if(y + height > Graphics2d.getClipUpperY()) {
            height -= y + height - Graphics2d.getClipUpperY();
        }

        if(width > 0 && height > 0) {
            fill(Graphics2d.getBuffer(), index, width, height, srcOff, destOff, srcStep, destStep, palette);
        }
    }

    private static void fill(int[] dest, byte[] index, int width, int height, int srcOff, int destOff, int srcStep, int destStep, int[] palette) {
        int steps = -(width >> 2);      // Write four pixels per step
        int rem = -(width & 3);

        for(int scan = -height; scan < 0; scan++) {
            for(int step = steps; step < 0; step++) {
                int i = index[srcOff++];
                if(i != 0) {
                    dest[destOff++] = palette[i & 0xff];
                } else {
                    destOff++;
                }

                i = index[srcOff++];
                if(i != 0) {
                    dest[destOff++] = palette[i & 0xff];
                } else {
                    destOff++;
                }

                i = index[srcOff++];
                if(i != 0) {
                    dest[destOff++] = palette[i & 0xff];
                } else {
                    destOff++;
                }

                i = index[srcOff++];
                if(i != 0) {
                    dest[destOff++] = palette[i & 0xff];
                } else {
                    destOff++;
                }
            }

            for(int step = rem; step < 0; step++) {
                int i = index[srcOff++];
                if(i != 0) {
                    dest[destOff++] = palette[i & 0xff];
                } else {
                    destOff++;
                }
            }

            srcOff += srcStep;
            destOff += destStep;
        }
    }

    public void draw(int x, int y, int width, int height) {
        if(width > 0 && height > 0) {
            int var5 = this.indexWidth;
            int var6 = this.indexHeight;
            int var7 = 0;
            int var8 = 0;
            int w = this.width;
            int h = this.height;
            int widthRatio = (w << 16) / width;
            int heightRatio = (h << 16) / height;
            int var13;
            if(this.offsetX > 0) {
                var13 = ((this.offsetX << 16) + widthRatio - 1) / widthRatio;
                x += var13;
                var7 += var13 * widthRatio - (this.offsetX << 16);
            }

            if(this.offsetY > 0) {
                var13 = ((this.offsetY << 16) + heightRatio - 1) / heightRatio;
                y += var13;
                var8 += var13 * heightRatio - (this.offsetY << 16);
            }

            if(var5 < w) {
                width = ((var5 << 16) - var7 + widthRatio - 1) / widthRatio;
            }

            if(var6 < h) {
                height = ((var6 << 16) - var8 + heightRatio - 1) / heightRatio;
            }

            var13 = x + y * Graphics2d.getWidth();
            int var14 = Graphics2d.getWidth() - width;
            if(y + height > Graphics2d.getClipLowerY()) {
                height -= y + height - Graphics2d.getClipUpperY();
            }

            int var15;
            if(y < Graphics2d.getClipLowerY()) {
                var15 = Graphics2d.getClipLowerY() - y;
                height -= var15;
                var13 += var15 * Graphics2d.getWidth();
                var8 += heightRatio * var15;
            }

            if(x + width > Graphics2d.getClipUpperX()) {
                var15 = x + width - Graphics2d.getClipUpperX();
                width -= var15;
                var14 += var15;
            }

            if(x < Graphics2d.getClipLowerX()) {
                var15 = Graphics2d.getClipLowerX() - x;
                width -= var15;
                var13 += var15;
                var7 += widthRatio * var15;
                var14 += var15;
            }

            method670(Graphics2d.getBuffer(), index, this.palette, 0, var7, var8, var13, var14, width, height, widthRatio, heightRatio, var5);
        }
    }

    private static final void method670(int[] dest, byte[] index, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
        int var12 = var3;

        for(int var13 = -var8; var13 < 0; ++var13) {
            int var14 = (var4 >> 16) * var11;

            for(int var15 = -var7; var15 < 0; ++var15) {
                var2 = var1[index[(var3 >> 16) + var14] & 0xff];
                if(var2 != 0) {
                    dest[var5++] = var2;
                } else {
                    ++var5;
                }

                var3 += var9;
            }

            var4 += var10;
            var3 = var12;
            var5 += var6;
        }

    }
}

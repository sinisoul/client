package com.jagex;

/**
 * Created by hadyn on 4/27/15.
 */
public class ChildEntry {
    int id = -1;
    int name = -1;

    ChildEntry() {}

    public void setId(int id) {
        this.id = id;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getName() {
        return name;
    }
}

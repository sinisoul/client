package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class SubNode extends Node {
    SubNode nextSubNode;
    SubNode prevSubNode;

    public void unlinkSubNode() {
        if(prevSubNode != null) {
            prevSubNode.nextSubNode = nextSubNode;
            nextSubNode.prevSubNode = prevSubNode;
            nextSubNode = null;
            prevSubNode = null;
        }
    }
}

package com.jagex;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by hadyn on 5/17/2015.
 */
public class Signlink implements Runnable {

    private String javaVendor;
    private String javaVersion;
    private String osName;
    private String osArch;
    private String osVersion;
    private String userHome;

    private String rootDirectory;
    private String fileDirectory;

    private FileOnDisk[] indexFiles;
    private FileOnDisk mainFile;
    private FileOnDisk tableIndexFile;
    private FileOnDisk randomFile;
    private File rootDir;

    private SignlinkRequest currentRequest;
    private SignlinkRequest lastRequest;
    private Thread thread;

    public Signlink(String game, int storeId, int indexCount) {
        javaVendor = "Unknown";
        javaVersion = "1.1";
        osName = "Unknown";
        osArch = "";

        // Capture the profile of the machine the client is running on,
        // it is possible for a security exception to be thrown for
        // any of the properties used to profile the machine.
        try {
            javaVendor = System.getProperty("java.vendor");
            javaVersion = System.getProperty("java.version");
        } catch(SecurityException ex) {}

        try {
            osName = System.getProperty("os.name");
        } catch(SecurityException ex) {}

        try {
            osArch = System.getProperty("os.arch");
        } catch(SecurityException ex) {}

        try {
            osVersion = System.getProperty("os.version");
        } catch(SecurityException ex) {}

        try {
            userHome = System.getProperty("user.home");
            if(userHome != null) {
                userHome += '/';
            }
        } catch(SecurityException ex) {}

        if(userHome == null) {
            userHome = "~/";                                    // Unix style?
        }

        // Initialize the game file store
        initializeFileStore(game, storeId, indexCount);

        // Create and start the thread which will serve the requests
        thread = new Thread(this);
        thread.setPriority(10);
        thread.setDaemon(true);
        thread.start();
    }

    public void initializeFileStore(String game, int storeId, int indexCount) {
        determineFileStorePath(game, storeId);
        try {
            File root = new File(rootDirectory);
            if(!root.exists()) {
                if(!root.mkdir()) {
                    return;                                 // TODO: Throw exception instead?
                }
            }

            if(randomFile == null) {
                File f = new File(root, "random.dat");
                if(!f.exists()) {
                    f.createNewFile();
                }
                if(f.exists()) {
                    randomFile = new FileOnDisk(f, "rw", 25L);
                }
            }

            if(rootDir == null) {
                try {
                    File fileDir = new File(fileDirectory);
                    if(!root.exists()) {
                        if(!root.mkdir()) {
                            return;                                 // TODO: Throw exception instead?
                        }
                    }

                    // If the main cache file doesn't exist create it
                    File main = new File(fileDir, "main_file_cache.dat2");
                    if (!main.exists()) {
                        main.createNewFile();
                    }
                    mainFile = new FileOnDisk(main, "rw", 1048576000000L);

                    // Create each of the index files
                    indexFiles = new FileOnDisk[indexCount];
                    for(int id = 0; id < indexCount; id++) {
                        indexFiles[id] = new FileOnDisk(new File(fileDir, "main_file_cache.idx" + id), "rw", 1048576L);
                    }
                    tableIndexFile = new FileOnDisk(new File(fileDir, "main_file_cache.idx255"), "rw", 1048576L);
                    rootDir = root;
                } catch(IOException ex) {
                    try {
                        mainFile.close();
                        for(int id = 0; id < indexCount; id++) {
                            indexFiles[id].close();
                        }
                        tableIndexFile.close();
                    } catch(IOException ioex) {}

                    // Null all of the file store objects just to reset
                    // their state.
                    rootDir = null;
                    randomFile = null;
                    mainFile = null;
                }
            }
        } catch(Throwable t) {

        }

        // This is in the client, I don't know if this is the best way of doing things but whatever. Yolo.
        if (rootDir == null)
            throw new RuntimeException();
    }

    /**
     *
     * @param game The name of the game for the file store directory we are looking up.
     * @param storeId The storage id.
     */
    private void determineFileStorePath(String game, int storeId) {
        if (storeId < 32 || storeId > 34) {
            storeId = 32;
        }

        // Parent and child directories in descending priority
        String[] parentDirs = { userHome, "./", };
        String[] childDirs = { "tekk_file_store_" + storeId };

        for(int parentId = 0; parentId < parentDirs.length; parentId++) {
            for (int childId = 0; childId < childDirs.length; childId++) {

                // First before we just attempt to create the directory
                // try testing if the priority parent directory exists
                // so that we can use it.
                String path = parentDirs[parentId] + childDirs[childId];
                if(path.length() > 0) {
                    File file = new File(path);
                    if(!file.exists() && parentId < 1) {
                        continue;
                    }
                }

                // Attempt to create the root file store path, or just
                // return the location if the path already exists.
                File file = new File(path);
                if (file.exists() || file.mkdir()) {
                    if (game.length() > 0) {
                        file = new File(file, game);
                        if (!file.exists() && !file.mkdir()) {
                            continue;
                        }
                    }
                    rootDirectory = file.getParent() + '/';
                    fileDirectory = file.getPath() + '/';
                    return;
                }
            }
        }
        throw new RuntimeException("Failed to create file store directory");
    }

    public SignlinkRequest createThread(Runnable runnable, int priority) {
        SignlinkRequest request = new SignlinkRequest(SignlinkRequest.CREATE_THREAD);
        request.setObjectArgument(runnable);
        request.setIntegerArgument(priority);
        submitRequest(request);
        return request;
    }

    public SignlinkRequest createSocket(String host, int port) {
        SignlinkRequest request = new SignlinkRequest(SignlinkRequest.CREATE_SOCKET);
        request.setObjectArgument(host);
        request.setIntegerArgument(port);
        submitRequest(request);
        return request;
    }

    private void submitRequest(SignlinkRequest request) {
        synchronized (this) {
            if(lastRequest != null) {
                lastRequest.setNext(request);
                lastRequest = request;
            } else {
                currentRequest = lastRequest = request;
            }
            notify();
        }
    }

    @Override
    public void run() {
        for(;;) {
            SignlinkRequest request;
            synchronized (this) {
                for(;;) {
                    if (false) {     // TODO: State variable
                        return;
                    }

                    // Poll the next request
                    if (currentRequest != null) {
                        request = currentRequest;
                        currentRequest = currentRequest.getNext();
                        if (currentRequest == null) {
                            lastRequest = null;
                        }
                        break;
                    }

                    try {
                        wait();
                    } catch (InterruptedException ex) {
                    }
                }
            }

            try {

                if(request.getType() == SignlinkRequest.CREATE_THREAD) {
                    Thread thread = new Thread((Runnable) request.getObjectArgument());
                    thread.setPriority(request.getIntegerArgument());
                    thread.start();
                    request.setResult(thread);
                }

                if(request.getType() == SignlinkRequest.CREATE_SOCKET) {
                    Socket socket = new Socket((String) request.getObjectArgument(), request.getIntegerArgument());
                    request.setResult(socket);
                }
                request.setStatus(SignlinkRequest.STATUS_DONE);
            } catch(Throwable t) {
                request.setStatus(SignlinkRequest.STATUS_ERROR);
            }
        }
    }
    /**
     *
     * @return
     */
    public String getJavaVendor() {
        return javaVendor;
    }

    /**
     *
     * @return
     */
    public String getJavaVersion() {
        return javaVersion;
    }

    /**
     *
     * @return
     */
    public FileOnDisk[] getIndexFiles() {
        return indexFiles;
    }

    public FileOnDisk getMainFile() {
        return mainFile;
    }

    public FileOnDisk getTableFile() {
        return tableIndexFile;
    }

    public FileOnDisk getRandomFile() {
        return randomFile;
    }
}

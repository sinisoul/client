package com.jagex;

/**
 * Created by hadyn on 5/17/2015.
 */
public class ThreadUtil {

    public static void sleep(long time) {
        if (time > 0L) {
            if (time % 10L == 0L) {
                threadSleep(time - 1L);
                threadSleep(1L);
            } else {
                threadSleep(time);
            }
        }
    }

    private static void threadSleep(long time) {
        try {
            Thread.sleep(time);
        } catch(InterruptedException ex) {}
    }
}

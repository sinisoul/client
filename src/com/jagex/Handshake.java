package com.jagex;

/**
 * Created by hadyn on 5/20/2015.
 */
public class Handshake {
    public static final int OP_UPDATE_SERVER = 15;
    public static final int STATUS_OKAY = 0;
    public static final int STATUS_OUT_OF_DATE = 6;
}

package com.jagex;

import java.io.IOException;
import java.util.zip.CRC32;

/**
 * Created by hadyn on 4/27/15.
 */
public class VolumeMetaTable {

    public static final int NAME_FLAG = 0x1;

    private static final CRC32 crc = new CRC32();

    private int[] ids;
    private TableEntry[] entries;
    private LinearHashTable names;
    private int length;
    private int size;
    private int revision;
    private int flags;

    public VolumeMetaTable(byte[] bytes, int expectedChecksum) {
        crc.reset();
        crc.update(bytes);
        int checksum = (int) crc.getValue();
        if(checksum != expectedChecksum) {
            throw new RuntimeException("CRC mismatch");
        }

        try {
            parse(EntryContainer.unpackData(bytes));
        } catch (IOException ex) {
            throw new RuntimeException();
        }
    }

    public void parse(byte[] bytes) {
        Buffer buffer = new Buffer(bytes);

        // Read the table version
        int version = buffer.getUInt8();
        if(version != 5 && version != 6) {
            throw new RuntimeException("Invalid version: " + version);
        }

        // For table versions 6 or greater read the revision
        if(version > 5) {
            revision = buffer.getInt32();
        } else {
            revision = 0;
        }

        // Read the flags attribute
        flags = buffer.getUInt8();

        // Read the size of the table, or the amount of entries
        size = buffer.getUInt16();

        // Calculate the length of the table and the id of each of the entries
        ids = new int[size];
        int counter = 0, max = -1;
        for(int i = 0; i < size; i++) {
            int id = ids[i] = counter += buffer.getUInt16();
            if(id > max) {
                max = id;
            }
        }
        length = max + 1;

        // Create each of the table entries and set their id,
        // if the entires are named read the name hash and set
        // the attribute
        entries = new TableEntry[length];
        for(int i = 0; i < size; i++) {
            TableEntry entry = entries[ids[i]] = new TableEntry();
            entry.setId(ids[i]);
            if((flags & NAME_FLAG) != 0) {
                entry.setName(buffer.getInt32());
            }
        }

        // Initialize the name hash table if needed
        if((flags & NAME_FLAG) != 0) {
            int[] arr = new int[size];
            for(int i = 0; i < size; i++) {
                TableEntry entry = entries[ids[i]];
                arr[i] = entry.getName();
            }
            names = new LinearHashTable(arr);
        }

        // Read and set entry checksum attribute
        for(int i = 0; i < size; i++) {
            entries[ids[i]].setCrc(buffer.getInt32());
        }

        // Read and set the entry version attribute
        for(int i = 0; i < size; i++) {
            entries[ids[i]].setVersion(buffer.getInt32());
        }

        // Read and set the amount of children for each entry,
        // calculate the length of the child id byte block
        // so that if we need to read the identifiers for
        // each child we have the appropriate offset
        int blockLen = 0;
        for(int i = 0; i < size; i++) {
            TableEntry entry = entries[ids[i]];
            entry.setSize(buffer.getUInt16());
            blockLen += entry.size() * 2;
        }

        // If the table has names for each of the children
        // calculate the offset in the buffer where the names
        // for the children start so we can read then when
        // we parse the children
        int[] namePositions = new int[size];
        for(int i = 0; i < size; i++) {
            namePositions[i] = -1;
            if((flags & NAME_FLAG) != 0) {
                TableEntry entry = entries[ids[i]];
                namePositions[i] = buffer.position() + blockLen;
                blockLen += entry.size() * 4;
            }
        }

        // Parse each of the table entries
        for(int i = 0; i < size; i++) {
            TableEntry entry = entries[ids[i]];
            entry.parseChildren(buffer, namePositions[i]);
        }
    }

    public boolean contains(int entryId) {
        return entryId >= 0 && entryId < length;
    }

    public int getRevision() {
        return revision;
    }

    public TableEntry get(int entryId) {
        return entries[entryId];
    }

    public int length() {
        return length;
    }

    public int size() {
        return size;
    }

    public int[] getEntryIds() {
        return ids;
    }

    public int getId(String name) {
        return names.get(Dbj2.hashOf(name));
    }
}

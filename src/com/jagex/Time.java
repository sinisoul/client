package com.jagex;

/**
 * Created by hadyn on 5/17/2015.
 */
public class Time {

    private static long previous;
    private static long delta;

    public static synchronized long currentTimeMillis() {
        long current = System.currentTimeMillis();
        if (previous > current) {
            delta += previous - current;
        }
        previous = current;
        return current + delta;
    }
}


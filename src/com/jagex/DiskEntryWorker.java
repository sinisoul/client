package com.jagex;

import java.io.IOException;

/**
 * Created by hadyn on 5/20/2015.
 */
public class DiskEntryWorker implements Runnable {

    private Queue<DiskEntryRequest> requests = new Queue<>();
    private Thread thread;
    private int size;

    public DiskEntryWorker(Signlink signlink) {
        SignlinkRequest request = signlink.createThread(this, 5);
        while(!request.isComplete()) {
            ThreadUtil.sleep(10L);
        }
        thread = (Thread) request.getResult();
    }

    public DiskEntryRequest immediatelyRead(Volume volume, int entryId) {
        DiskEntryRequest request = new DiskEntryRequest(DiskEntryRequest.READ_IMMEDIATELY, volume, entryId);
        synchronized (requests) {
            for(DiskEntryRequest compare = requests.first(); compare != null; compare = requests.next()) {
                if(compare.getEntryId() == entryId && compare.getType() == DiskEntryRequest.WRITE) {
                    request.setBytes(compare.getBytes());
                    request.setComplete(true);
                    return request;
                }
            }
        }
        request.setBytes(volume.read(entryId));
        request.setPriority(true);
        request.setComplete(true);
        return request;
    }

    public DiskEntryRequest read(Volume volume, int entryId) {
        DiskEntryRequest request = new DiskEntryRequest(DiskEntryRequest.READ, volume, entryId);
        appendRequest(request);
        return request;
    }

    public DiskEntryRequest write(Volume volume, int entryId, byte[] bytes) {
        System.out.println("Writing to disk " + volume.getId() + ", " + entryId);
        DiskEntryRequest request = new DiskEntryRequest(DiskEntryRequest.WRITE, volume, entryId);
        request.setBytes(bytes);
        appendRequest(request);
        return request;
    }

    private void appendRequest(DiskEntryRequest request) {
        synchronized (requests) {
            requests.add(request);
            requests.notifyAll();
            size++;
        }
    }

    @Override
    public void run() {
        while(true) {

            // Poll for the next request, if there are no requests
            // then wait until we are alerted for a request
            DiskEntryRequest request;
            synchronized (requests) {
                request = requests.poll();
                if(request == null) {
                    try {
                        requests.wait();
                    } catch(InterruptedException ex) {
                    }
                    continue;
                }
                size--;
            }

            try {
                Volume volume = request.getVolume();

                // For entries needing to be read from a file store volume,
                // read the bytes for the request, set the request bytes
                // and, set the request as complete
                if(request.getType() == DiskEntryRequest.READ) {
                    request.setBytes(volume.read(request.getEntryId()));
                }

                // For entries needing to be written to a file store volume,
                // write the bytes for the request and then set the request
                // as complete.
                if(request.getType() == DiskEntryRequest.WRITE) {
                    volume.write(request.getEntryId(), request.getBytes());
                }

                // Mark the request as completed
                request.setComplete(true);
            } catch(IOException ex) {}
        }
    }

    public int size() { return size; }
}


package com.jagex;

import java.io.IOException;

/**
 * Created by hadyn on 5/19/2015.
 */
public class FileIndex {
    private static final int[] BLANK_KEYS = new int[4];
    private AbstractIndexRequester requester;
    private AbstractByteArray[] entries;
    private AbstractByteArray[][] children;
    private VolumeMetaTable table;
    private boolean directBufferEntries;
    private boolean directBufferChildren;

    public FileIndex(AbstractIndexRequester requester, boolean directBufferEntries, boolean directBufferChildren) {
        this.requester = requester;
        this.directBufferEntries = directBufferEntries;
        this.directBufferChildren = directBufferChildren;
    }

    public boolean loadAll() {
        if(!isReferenceTableLoaded()) {
            return false;
        }

        boolean bool = true;
        int[] entryIds = table.getEntryIds();
        for(int i = 0; i < entryIds.length; i++) {
            if(entries[entryIds[i]] == null) {
                load(entryIds[i]);
                if(entries[entryIds[i]] == null) {
                    bool = false;
                }
            }
        }
        return bool;
    }

    public boolean loadEntry(int entryId) {
        if(!exists(entryId)) {
            return false;
        }
        // If the entry is not yet loaded attempt to load it
        // and return if the entry was successfully loaded
        if(entries[entryId] == null) {
            load(entryId);
            return entries[entryId] != null;
        }
        return true;
    }

    public boolean contains(String name) {
        return false;
    }

    public byte[] get(String parent, String child) {
        return null;
    }

    public byte[] get(int entryId, int childId) {
        return get(entryId, childId, BLANK_KEYS);
    }

    public byte[] get(int entryId, int childId, int[] keys) {
        if(!exists(entryId, childId)) {
            return null;
        }
        if(children[entryId] == null || children[entryId][childId] == null) {
            boolean successful = unpack(entryId, keys);
            if(!successful) {
                load(entryId);
                successful = unpack(entryId, keys);
                if(!successful) {
                    return null;
                }
            }
        }

        AbstractByteArray arr = children[entryId][childId];
        if(!directBufferChildren) {
            TableEntry entry = table.get(entryId);
            children[entryId][childId] = null;
            if(entry.size() == 1) {
                entries[entryId] = null;
            }
        }

        return arr.get();
    }

    public int getProgress(int entryId) {
        if(!exists(entryId)) {
            return 0;
        }
        return entries[entryId] != null ? 100 : requester.getProgress(entryId);
    }

    public int getProgress(String name) {
        return -1;
    }

    private boolean isReferenceTableLoaded() {
        if(table == null) {
            table = requester.getReferenceTable();
            if(table == null) {
                return false;
            }
            entries = new AbstractByteArray[table.length()];
            children = new AbstractByteArray[table.length()][];
        }
        return true;
    }

    private boolean exists(int entryId) {
        if(isReferenceTableLoaded()) {
            if(entryId >= 0 && entryId < table.length() && table.contains(entryId)) {
                return true;
            }
        }
        return false;
    }

    private boolean exists(int entryId, int childId) {
        if(isReferenceTableLoaded()) {
           if(entryId >= 0 && childId >= 0 && table.contains(entryId) && table.get(entryId).length() > childId) {
               return true;
           }
        }
        return false;
    }

    private void load(int entryId) {
        byte[] bytes = requester.get(IndexRequester.REMOTE_REQUEST_PRIORITY, entryId);
        if(bytes == null) {
            return;
        }

        if(directBufferEntries) {
            entries[entryId] = DirectByteBuffer.create(bytes);
        } else {
            entries[entryId] = WrappedByteArray.create(bytes);
        }
    }

    private boolean unpack(int entryId) {
        return unpack(entryId, BLANK_KEYS);
    }

    private boolean unpack(int entryId, int[] keys) {
        if(!exists(entryId)) {
            return false;
        }

        if(entries[entryId] == null) {
            return false;
        }

        TableEntry entry = table.get(entryId);
        int[] childIds = entry.getChildIds();
        int length = entry.length();
        int size = childIds.length;

        AbstractByteArray[] buffers;
        if(children[entryId] == null) {
            children[entryId] = new AbstractByteArray[length];
        }
        buffers = children[entryId];

        boolean loaded = true;
        for(int i = 0; i < childIds.length; i++) {
            if(buffers[childIds[i]] == null) {
                loaded = false;
            }
        }

        if(loaded) {
            return true;
        }

        byte[] packed;
        if(keys != null && (keys[0] != 0 || keys[1] != 0 || keys[2] != 0 || keys[3] != 0)) {
            packed = entries[entryId].copy();
            Buffer buffer = new Buffer(packed);
            // TODO: Decrypt the entry
        } else {
            packed = entries[entryId].get();
        }

        byte[] bytes;
        try {
            bytes = EntryContainer.unpackData(packed);
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }

        if(!directBufferEntries) {
            entries[entryId] = null;
        }

        if(size > 1) {
            int pos = bytes.length;
            pos--;

            int steps = bytes[pos] & 0xff;
            pos -= 4 * size * steps;

            Buffer buffer = new Buffer(bytes);
            buffer.position(pos);

            int[] offsets = new int[size];

            int len;
            for(int step = 0; step < steps; step++) {
                len = 0;
                for(int i = 0; i < size; i++) {
                    len += buffer.getInt32();
                    offsets[i] += len;
                }
            }

            byte[][] childBuffers = new byte[size][];
            for(int index = 0; index < size; index++) {
                childBuffers[index] = new byte[offsets[index]];
                offsets[index] = 0;
            }

            buffer.position(pos);

            int off = 0;
            for(int step = 0; step < steps; step++) {
                len = 0;
                for(int i = 0; i < size; i++) {
                    len += buffer.getInt32();
                    System.arraycopy(bytes, off, childBuffers[i], offsets[i], len);
                    offsets[i] += len;
                }
            }

            for(int i = 0; i < size; i++) {
                if(directBufferChildren) {
                    buffers[childIds[i]] = DirectByteBuffer.create(childBuffers[i]);
                } else {
                    buffers[childIds[i]] = WrappedByteArray.create(childBuffers[i]);
                }
            }
        } else {
            if(directBufferChildren) {
                buffers[childIds[0]] = DirectByteBuffer.create(bytes);
            } else {
                buffers[childIds[0]] = WrappedByteArray.create(bytes);
            }
        }
        return true;
    }

    public int getProgress() {
        if(!isReferenceTableLoaded()) {
            return 0;
        }

        int[] entryIds = table.getEntryIds();

        int count = 0;
        int total = 0;

        for(int i = 0; i < entryIds.length; i++) {
            count += 100;
            total += getProgress(entryIds[i]);
        }
        if(count != 0) {
            return total * 100 / count;
        }
        return 100;
    }

    public int getId(String name) {
        if(!isReferenceTableLoaded()) {
            return -1;
        }
        int id = table.getId(name);
        return exists(id) ? id : -1;
    }
}
